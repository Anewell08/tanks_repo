﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTankBoostAbility : AbilityScript
{


    public override void Start()
    {
        base.Start();
        abilityTextString = "Increase movement speed by 50% for " + Mathf.Round(5f * levelBoostPercentage) + " seconds.";
    }

    public override void OnLeft()
    {
        Debug.Log("On left boost");
        if (countdown <=0)
        {
            StartCoroutine("Boost");
            
        }        
    }

    public override void UpdateLevel(int unitPlayerLevel)
    {
        base.UpdateLevel(unitPlayerLevel);        
        abilityTextString = "Increase movement speed by 50% for " + Mathf.Round(5f * levelBoostPercentage) + " seconds.";
    }

    public IEnumerator Boost()
    {
        
        float resetSpeed = pManager.movementSpeed;
        pController.movementSpeed = pController.movementSpeed * 1.5f;
        yield return new WaitForSeconds(5f * levelBoostPercentage);
        pController.movementSpeed = resetSpeed;
        countdown = 1f / fireRate;
    }
}
