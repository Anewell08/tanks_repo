﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityGrabber : MonoBehaviour
{
    public GameObject target;
    public AbilityScript targetScript;
    public Image icon;

    public void OnLeft()
    {
        targetScript.OnLeft();

    }

    public void OnRight()
    {
        targetScript.OnRight();
    }

    public void SetImage()
    {
        icon.sprite = targetScript.icon.sprite;

    }

}
