﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyItemScript : Photon.MonoBehaviour
{

    private Item inventorySlotItem;
    public InventorySlot inventorySlot;

    PhotonView playerPV;
    PlayerManager playerPM;
    GameObject playerGO;

    public GameObject targetShop;

    public GameObject parentObject;

    private void Start()
    {
        inventorySlotItem = inventorySlot.item;
        targetShop = parentObject.GetComponent<GetTargetShop>().target;
    }

    

    public void BuyItem()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (player.GetComponent<PhotonView>().isMine)
            {
                playerPV = player.GetComponent<PhotonView>();
                playerPM = player.GetComponent<PlayerManager>();
                playerGO = player;
            }
        }
        
        if (playerPV != null && playerPV.isMine)
        {
            if (playerPM.currency > inventorySlotItem.cost)
            {
                if (Vector3.Distance(playerGO.transform.position, targetShop.transform.position) < 10)
                {
                    playerPM.currency -= inventorySlotItem.cost;
                    playerPV.RPC("PunAdd", PhotonTargets.AllViaServer, inventorySlotItem.dictionaryString, 0);
                }
                else
                {
                    //Too far away!
                    Debug.Log("Too far away from shop");
                }
                
            }
            else
            {
                //Not enough currency to buy!
                Debug.Log("Not enough currency");
            }
            
        }

    }

}
