﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;


public class UnitManager : Photon.PunBehaviour , IPunObservable
{

    #region Variables
    [Tooltip("The Player's UI GameObject Prefab")]
    public GameObject UnitUiPrefab;             // UI object that is instantiated on spawn
    public Animator anim;

    [Header("Unit Attributes")]
    public float maxHealth = 300;               // Max health of the unit, set in the inspector and other scripts draw from this float
    public float followDistance;                // Follow range of the unit
    public bool IsMovingUnit;                   // (Unused right now) Used to not allow units that arent supposed to move to move.
    public float movementSpeed = 5;             // Movement speed of the unit    
    public int bounty = 10;

    [Header("Setup Fields")]                    // Setup the firepoint and part to rotate for the unit that the weapon will draw from.
    public Transform partToRotate;
    public Transform partToRotateChildBegRot;
    public Transform firepoint;

    public Transform gunTransform;
    public Transform shootingTransform;
    public Transform runningTransform;

    [HideInInspector]
    public bool hasTarget = false;              // Bool that tells the unit to move along waypoint system if it has no target. Set from tank weapon.
    [HideInInspector]
    public int unitLevel = 1;
    [HideInInspector]
    public bool UnitUIOn = true;
    [HideInInspector]
    public bool UnitUICreated = false;

    NavMeshAgent agent;                         // Reference to nav mesh agent
    protected TeamManager teamManager;
    protected GameManager gameManager;

    Vector2 smoothDeltaPosition = Vector2.zero;
    Vector2 velocity = Vector2.zero;


    private Vector3 latestCorrectPos;
    private Vector3 onUpdatePos;
    private Vector3 latestCorrectPosGun;
    private Vector3 onUpdatePosGun;
    private Quaternion latestCorrectRot;
    private Quaternion onUpdateRot;
    private Quaternion latestCorrectRotGun;
    private Quaternion onUpdateRotGun;
    private bool isRunning;
    private bool isShooting;
    private float fraction;





    #endregion

    #region Start, Awake, Update

    public void Awake()
    {
        if (teamManager == null)  //Stores a reference to the Team Manager in order to call to it later to kill and respawn the player.
        {
            teamManager = TeamManager.Instance;
        }
        if (gameManager == null)
        {
            gameManager = GameManager.Instance;
        }

    }
    //// Use this for initialization
    public virtual void Start()
    {
        if (teamManager == null)  //Stores a reference to the Team Manager in order to call to it later to kill and respawn the player.
        {
            teamManager = TeamManager.Instance;
        }
        if (gameManager == null)
        {
            gameManager = GameManager.Instance;
        }
        if (UnitUiPrefab != null)
        {
            UnitUIOn = gameManager.UnitUIOn;
            if (UnitUIOn == true)
            {
                // Instantiates a unit UI prefab and sets the target of the prefab to this gameobject
                GameObject _uiGo = Instantiate(UnitUiPrefab) as GameObject;
                UnitUiPrefab = _uiGo;
                _uiGo.SendMessage("SetTarget", this);
                UnitUICreated = true;
            }
            
        }
        if (GetComponent<NavMeshAgent>() != null)
        {
            // Stores reference to the nav mesh agent on this unit
            agent = GetComponent<NavMeshAgent>();
            agent.speed = movementSpeed + 1;
        }
        
        // Sets level of unit based on current team level
        if (gameObject.GetComponent<TeamNumber>().teamNumber == 1) 
        {
            unitLevel = teamManager.currentLevelTeam1;
        }
        else
        {
            unitLevel = teamManager.currentLevelTeam2;
        }

    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (PhotonNetwork.isMasterClient)
        {
            
                //// We own this player: send the others our data
                //stream.SendNext(unitHealth);
                //stream.SendNext(transform.position);
                //stream.SendNext(transform.rotation);

                //Sets the variables to send then sends them to other players
                Vector3 pos = transform.position;
                Quaternion rot = transform.rotation;
                Vector3 posGun = gunTransform.position;
                Quaternion rotGun = gunTransform.rotation;
                bool isRunningStream = anim.GetBool("isRunning");
                bool isShootingStream = anim.GetBool("isShooting");

                stream.Serialize(ref pos);
                stream.Serialize(ref rot);
                stream.Serialize(ref posGun);
                stream.Serialize(ref rotGun);
                stream.Serialize(ref isRunningStream);
                stream.Serialize(ref isShootingStream);


                //int[] itemSendInt = new int[6];
                //for (int i = 0; i < itemSendInt.Length; i++)
                //{
                //    itemSendInt[i] = Inventory.items[i].dictionaryInt;
                //    stream.Serialize(ref itemSendInt[i]);
                //}





            
        }
        else
        {
            if (!stream.isWriting)
            {
                // Receive latest state information
                Vector3 pos = Vector3.zero;
                Quaternion rot = Quaternion.identity;
                Vector3 posGun = Vector3.zero;
                Quaternion rotGun = Quaternion.identity;
                bool isRunningStream = false;
                bool isShootingStream = true;


                stream.Serialize(ref pos);
                stream.Serialize(ref rot);
                stream.Serialize(ref posGun);
                stream.Serialize(ref rotGun);
                stream.Serialize(ref isRunningStream);
                stream.Serialize(ref isShootingStream);

                this.latestCorrectPos = pos;                // save this to move towards it in FixedUpdate()
                this.onUpdatePos = transform.position; // we interpolate from here to latestCorrectPos
                this.latestCorrectPosGun = posGun;                // save this to move towards it in FixedUpdate()
                this.onUpdatePosGun = gunTransform.position; // we interpolate from here to latestCorrectPos
                this.latestCorrectRot = rot;
                onUpdateRot = transform.rotation;
                this.latestCorrectRotGun = rotGun;
                onUpdateRotGun = gunTransform.rotation;
                isRunning = isRunningStream;
                isShooting = isShootingStream;
                this.fraction = 0;                          // reset the fraction we alreay moved. see Update()

                //transform.localRotation = rot;              // this sample doesn't smooth rotation

            }





        }
    }

    public virtual void Update ()
    {
        if (PhotonNetwork.isMasterClient)
        {
            if (!hasTarget)
            {
                // Copied AI movement script
                #region Copied AI Movement and Animation
                ////Movement of AI

                Vector3 worldDeltaPosition = agent.nextPosition - transform.position;

                // Map 'worldDeltaPosition' to local space
                float dx = Vector3.Dot(transform.right, worldDeltaPosition);
                float dy = Vector3.Dot(transform.forward, worldDeltaPosition);
                Vector2 deltaPosition = new Vector2(dx, dy);

                // Low-pass filter the deltaMove
                float smooth = Mathf.Min(1.0f, Time.deltaTime / 0.15f);
                smoothDeltaPosition = Vector2.Lerp(smoothDeltaPosition, deltaPosition, smooth);

                // Update velocity if time advances
                if (Time.deltaTime > 1e-5f)
                    velocity = smoothDeltaPosition / Time.deltaTime;

                bool shouldMove = (velocity.magnitude > 0.5f && agent.remainingDistance > agent.radius);

                //Animation
                // Update animation parameters
                anim.SetBool("isRunning", true);
                anim.SetBool("isShooting", false);
                if (gunTransform != null)
                {
                    gunTransform.position = Vector3.Lerp(gunTransform.position, runningTransform.position, Time.deltaTime * 3);
                    gunTransform.rotation = Quaternion.Lerp(gunTransform.rotation, runningTransform.rotation, Time.deltaTime * 3);
                }
                //anim.SetFloat("velx", velocity.x);
                //anim.SetFloat("vely", velocity.y);
                #endregion

                // Move unit based on its Nav Mesh Agents next position, set in WayPoint Navigator Script
                transform.position = agent.nextPosition;

                // Set rotation of target based on where the agent is headed
                Vector3 targetToLookAt = agent.steeringTarget + transform.forward; // Look forwards from target
                targetToLookAt.y = this.gameObject.transform.position.y;           // Make sure they aren't looking up or down
                Vector3 pos = targetToLookAt - transform.position;                 // Finds the difference between target and current position
                var newRot = Quaternion.LookRotation(pos);                         // Looks in the direction of the target
                transform.rotation = Quaternion.Lerp(transform.rotation, newRot, Time.deltaTime * 5);       // Lerps to target rotation
            }
            if (hasTarget)
            {
                anim.SetBool("isRunning", false);
                anim.SetBool("isShooting", true);
                if (gunTransform != null)
                {
                    gunTransform.position = Vector3.Lerp(gunTransform.position, shootingTransform.position, Time.deltaTime * 3);
                    gunTransform.rotation = Quaternion.Lerp(gunTransform.rotation, shootingTransform.rotation, Time.deltaTime * 3);
                }

            }
        }
        else
        {
            // We get 10 updates per sec. Sometimes a few less or one or two more, depending on variation of lag.
            // Due to that we want to reach the correct position in a little over 100ms. We get a new update then.
            // This way, we can usually avoid a stop of our interpolated cube movement.
            //
            // Lerp() gets a fraction value between 0 and 1. This is how far we went from A to B.
            //
            // So in 100 ms, we want to move from our previous position to the latest known. 
            // Our fraction variable should reach 1 in 100ms, so we should multiply deltaTime by 10.
            // We want it to take a bit longer, so we multiply with 9 instead!

            this.fraction = this.fraction + Time.deltaTime * 9.9f;
            transform.position = Vector3.Lerp(this.onUpdatePos, this.latestCorrectPos, this.fraction); // set our pos between A and B
            transform.rotation = Quaternion.Lerp(this.onUpdateRot, this.latestCorrectRot, this.fraction);    // set our rotation between A and B
            gunTransform.position = Vector3.Lerp(this.onUpdatePosGun, this.latestCorrectPosGun, this.fraction); // set our pos between A and B
            gunTransform.rotation = Quaternion.Lerp(this.onUpdateRotGun, this.latestCorrectRotGun, this.fraction);    // set our rotation between A and B
            anim.SetBool("isRunning", isRunning);
            anim.SetBool("isShooting", isShooting);


        }

    }

    public void SpawnUI()
    {
        if (UnitUiPrefab != null)
        {
            UnitUIOn = gameManager.UnitUIOn;
            if (UnitUIOn == true)
            {
                // Instantiates a unit UI prefab and sets the target of the prefab to this gameobject
                GameObject _uiGo = Instantiate(UnitUiPrefab) as GameObject;
                UnitUiPrefab = _uiGo;
                _uiGo.SendMessage("SetTarget", this);
                UnitUICreated = true;
            }

        }


    }


    public void LevelUp(int currentLevel)
    {
        // Update level
        unitLevel = currentLevel;
        // Update health
        gameObject.GetComponent<UnitHealth>().LevelUpHealth(unitLevel);

        //Update damage. Need to make sure I fix if i ever change the weapon from a tank weapon
        float tempDamage = gameObject.GetComponentInChildren<TankWeapon>().damage;
        tempDamage = Mathf.RoundToInt(tempDamage * (1 + ((float)unitLevel / 50)));        
        gameObject.GetComponentInChildren<TankWeapon>().damage = tempDamage;

    }
    #endregion
}
