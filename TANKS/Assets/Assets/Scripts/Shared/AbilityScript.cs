﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityScript : MonoBehaviour
{
    public Image icon;

    public PlayerManager pManager;
    public PlayerController pController;
    public string abilityTextString;

    public int currentLevel;
    public float levelBoostPercentage;

    public float countdown;
    public float fireRate = 0.1f; // Measured in times (activated or used) per second

    protected PhotonView playerPhotonView;

    public virtual void Start()
    {
        pManager = gameObject.GetComponent<PlayerManager>();
        pController = gameObject.GetComponent<PlayerController>();
        playerPhotonView = gameObject.GetPhotonView();
        currentLevel = pManager.playerLevel;
        StartBoostPercentage(currentLevel);
    }

    public void StartBoostPercentage(int level)
    {
        levelBoostPercentage = 1;
        for (int i = 1; i <= level; i++)
        {
            levelBoostPercentage = Mathf.RoundToInt(levelBoostPercentage * (1 + ((float)i / 60)));
        }
    }

    // This will run from clickable button script when left mouse is clicked or correct ability button
    public virtual void OnLeft()
    {
        Debug.Log("On left");


    }

    // This will run from clickable button script when right mouse is clicked or correct ability button
    public virtual void OnRight()
    {
        Debug.Log("On right");


    }

    public virtual void UpdateLevel(int unitPlayerLevel)
    {
        currentLevel = unitPlayerLevel;        
        levelBoostPercentage = levelBoostPercentage * (1 + ((float)currentLevel / 60));
        
    }

    public virtual void Update()
    {
        countdown -= Time.deltaTime;

    }


}
