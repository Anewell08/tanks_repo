﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamNumber : MonoBehaviour {

    // Finds the tag of the child object titled "Team" to determine team number.
    public int teamNumber;

    void Start ()
    {
        if (gameObject.transform.Find("MinimapObject").gameObject != null)
        {
            gameObject.transform.Find("MinimapObject").gameObject.GetComponent<MinimapColor>().SetColor();
        }

        if (PhotonNetwork.isMasterClient)
        {
            GetComponent<PhotonView>().RPC("PunUpdateTeamNumber", PhotonTargets.OthersBuffered, teamNumber);
        }
        
    }

    [PunRPC]
    public void PunUpdateTeamNumber(int tNumber)
    {
        teamNumber = tNumber;
    }




}
