﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{


    #region Public Properties


    [Tooltip("UI Text to display Player's Name")]
    public Text PlayerNameText;


    [Tooltip("UI Slider to display Player's Health")]
    public Slider PlayerHealthSlider;

    [Tooltip("Pixel offset from the player target")]
    public Vector3 ScreenOffset = new Vector3(0f, -60f, 0f);


    #endregion


    #region Private Properties
    PlayerManager _target;

    float _boxColliderHeight = 0f;
    private float _boxColliderZ;
    private float _boxColliderX;
    Transform _targetTransform;
    Vector3 _targetPosition;
    public float offset;

    #endregion


    #region MonoBehaviour Messages

    void Awake()
    {
        this.GetComponent<Transform>().SetParent(GameObject.Find("Canvas").GetComponent<Transform>());
        
        // Set Max Health

    }

    void Update()
    {
        // Destroy itself if the target is null, It's a fail safe when Photon is destroying Instances of a Player over the network
        if (_target == null)
        {
            Destroy(this.gameObject);
            return;
        }
        // Reflect the Player Health
        if (PlayerHealthSlider != null)
        {
            PlayerHealthSlider.maxValue = _target.GetComponent<UnitHealth>().maxHealth;
            PlayerHealthSlider.value = _target.GetComponent<UnitHealth>().currentHealth;
        }

    }

    #endregion


    #region Public Methods



    public void SetTarget(PlayerManager target)
    {
        if (target == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> PlayMakerManager target for PlayerUI.SetTarget.", this);
            return;
        }
        // Cache references for efficiency
        _target = target;
        if (PlayerNameText != null)
        {
            PlayerNameText.text = _target.PhotonView.owner.name;
        }

        // Get data from the Player that won't change during the lifetime of this Component
        BoxCollider _boxCollider = _target.GetComponent<BoxCollider>();
        if (_boxCollider != null)
        {
            _boxColliderHeight = _boxCollider.size.y;
            _boxColliderZ = _boxCollider.size.z;
            _boxColliderX = _boxCollider.size.x;
        }
        if (PlayerHealthSlider != null)
        {
            PlayerHealthSlider.maxValue = _target.GetComponent<PlayerManager>().maxHealth;
        }
        ScreenOffset.y = ScreenOffset.y + Screen.height * offset;
    }

    void LateUpdate()
    {
        // #Critical
        // Follow the Target GameObject on screen.
        if (_target != null)
        {
            _targetPosition = _target.transform.position;
            _targetPosition.y -= Mathf.Max(_boxColliderZ, _boxColliderX, _boxColliderHeight);
            this.transform.position = Camera.main.WorldToScreenPoint(_targetPosition) + ScreenOffset;
        }
        
            
        
    }

    #endregion


}

