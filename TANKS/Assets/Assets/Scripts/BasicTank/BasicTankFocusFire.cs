﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTankFocusFire : AbilityScriptSingleTarget {

    public override void Start()
    {
        base.Start();
        abilityTextString = "The tank focuses all of its weapons in its inventory on the unit selected. The weapon will lose the target if the selected unit moves out of range of the weapon.";
    }

    public override void UpdateLevel(int unitPlayerLevel)
    {
        base.UpdateLevel(unitPlayerLevel);
        fireRate = fireRate * levelBoostPercentage;
        abilityTextString = "The tank focuses all of its weapons in its inventory on the unit selected. The weapon will lose the target if the selected unit moves out of range of the weapon.";
    }

    public override void StartAbilityCoroutine()
    {
        StartCoroutine("SelectUnit");
    }

    public IEnumerator SelectUnit()
    {
        TankWeapon[] tankWeapons = GetComponentsInChildren<TankWeapon>();

        foreach (TankWeapon weapon in tankWeapons)
        {
            weapon.target = interactable.gameObject.transform;
        }
        yield return new WaitForEndOfFrame();
    }


}
