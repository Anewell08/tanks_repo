﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimation : MonoBehaviour
{
    FindTarget findTarget;

    private void Awake()
    {
        if (GetComponent<FindTarget>() != null)
        {
            findTarget = GetComponent<FindTarget>();
        }
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        if (findTarget.target == null)
        {

        }
        else if(findTarget.target != null)
        {
            //if in range, play shooting animation
            //else, running animation
        }
    }

}
