﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitController : Photon.MonoBehaviour, IPunObservable
{
    //This script should only do movement and rotation

    public float movementSpeed;

    //reference variables
    NavMeshAgent agent;
    Rigidbody rBody;
    FindTarget findTarget;


    //Sync variables
    private float lastSynchronizationTime = 0f;
    private float syncDelay = 0f;
    private float syncTime = 0f;
    private Vector3 syncStartPosition = Vector3.zero;
    private Vector3 syncEndPosition = Vector3.zero;
    private Quaternion syncStartRotation = Quaternion.identity;
    private Quaternion syncEndRotation = Quaternion.identity;


    private bool _isLerping;
    private float _timeStartedLerping;
    private float timeTakenDuringLerp = 1;
    private bool _isLerpingSync;
    private bool _isLerpingMaster;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        rBody = GetComponent<Rigidbody>();
        findTarget = GetComponent<FindTarget>();
    }

    private void Start()
    {
        syncStartPosition = transform.position;
        syncEndPosition = transform.position;

    }





    private void FixedUpdate()
    {

        if (findTarget.target == null)
        {
            if (PhotonNetwork.isMasterClient)
            {
                Movement();
                
            }
            else
            {
                SyncedMovement();
            }
        }
        else // we have a target, so don't allow nav mesh agent to move
        {
            agent.isStopped = true;
            //LookTowardsTarget();
            //if looking at target, and in range, set bool to true for weapon script

            //if looking towards,
            //MoveTowardsTarget();
        }
    }



    private void SyncedMovement()
    {

        syncTime += Time.deltaTime;
        transform.localPosition = Vector3.Lerp(syncStartPosition, syncEndPosition, (syncTime / syncDelay));
        transform.localRotation = Quaternion.Lerp(syncStartRotation, syncEndRotation, (syncTime / syncDelay));
    }

    private void Movement()
    {
        rBody.MovePosition(Vector3.Lerp(rBody.position, agent.nextPosition + transform.forward * (movementSpeed/100), 5 * Time.deltaTime));
        agent.nextPosition = transform.position;
        agent.isStopped = false;
    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //// We own this player: send the others our data

            //Sets the variables to send then sends them to other players

            // Position
            Vector3 pos = transform.localPosition;
            Quaternion rot = transform.localRotation;
            Vector3 velo = rBody.velocity;
            stream.Serialize(ref pos);
            stream.Serialize(ref rot);
            stream.Serialize(ref velo);

            
        }
        else
        {
            //// Network player, receive data

            // Receive latest state information
            Vector3 pos = Vector3.zero;
            Quaternion rot = Quaternion.identity;
            Vector3 velo = Vector3.zero;

            stream.Serialize(ref pos);
            stream.Serialize(ref rot);
            stream.Serialize(ref velo);

            // find sync
            syncTime = 0f;
            syncDelay = Time.time - lastSynchronizationTime;
            lastSynchronizationTime = Time.time;
            Debug.Log(velo);
            Debug.Log(velo * syncDelay + "velo with sync delay");
            //update sync position and rotation
            syncEndPosition = pos + velo * syncDelay;            
            syncEndRotation = rot;
            syncStartPosition = transform.localPosition;
            syncStartRotation = this.transform.localRotation;

            

            
        }
    }

}
