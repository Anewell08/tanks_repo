﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TankWeapon : MonoBehaviour {

    #region Variables
    protected bool lookTarget;                  // Target to look at
    protected int enemyTeamNumber;              // Not used on this script, maybe on fireball cannon
    protected int playerTeamNumber;             // Not used on this script, maybe on fireball cannon
    protected float targetDistance;             // Float used to find the distance to the target
    protected NavMeshAgent agent;               // reference to the nav mesh agent of units
    protected bool hasNavMeshAgent;             // Bool to know whether the unit has a nav mesh agent
    
    

    [HideInInspector]
    public int teamNumber;                      // Team number of the unit
    [HideInInspector]
    public float movementSpeed;                 // Movement speed of unit, received from player manager
    [HideInInspector]
    public float followDistance;                // Follow distance of unit, received from player manager
    [HideInInspector]
    public Transform target;                    // Received from nearest target variable. Target of this weapon
    [HideInInspector]
    public bool IsMovingUnit;                   // Bool to determine if this unit should move
    [HideInInspector]
    public GameObject parentObject;             // Reference to parent object

    // References to object managers
    [HideInInspector]
    public PlayerManager parentObjectPlayerManager;
    [HideInInspector]
    public UnitManager parentObjectUnitManager;

    PhotonView photonView;

    [Header("Attributes")]
    // Attributes of the weapon. Fire rate is measured in shots per second
    protected float fireCountdown = 0f;
    public float range = 15f;
    [Tooltip("Fire rate is measured in shots per second.")]
    public float fireRate = 1f;
    public float damage = 50;
    public float cost = 1125; // Cost of the weapon in currency




    [Header("UnitySetupFields")]

    public float turnSpeed = 10;                    // How quickly the part to rotate rotates
    // Bool to know whether or not it is the primary weapon in order to know whether the unit needs to rotate its part to rotate. We don't want to rotate if this is our second weapon.
    public bool isPrimaryWeapon = false;            // Also set to false as most weapons created will not be the primary weapon
    protected Transform partToRotate;               // The part that needs to rotate. Received from player or unit manager
    protected Transform partToRotateChildBegRot;    // Beginning rotation of the part to rotate. Received from player or unit manager
    public GameObject nearestTarget;                // nearest player and nearest enemy are used to set this
    public GameObject bulletPrefab;                 // Bullet prefab to shoot
    protected Transform firepoint;                  // Point where the bullet will leave from
    private float lookYTarget;
    public bool lookY = false;
    private string bulletPrefabName;


    #endregion

    // Notes on scripts derived from this one
    // We only want the default weapon to rotate the unit, so override the the rotate functions. Everything else stays the same
    // The default weapon will also set and change the player manager/ unit managers nav mesh agent, so this will too
    // This is fine, because there will be instances where the default weapon is out of range but this one is not, so it will make sure the agent is not turned back on
    // Besides, most non-player units will only have one weapon, so the agent will be changed on only one weapon

    // Use this for initialization
    public virtual void Start ()
    {
        bulletPrefabName = bulletPrefab.name;
        //Sets the team number, parent object, parent object plaer manager, how far the follow distance should be (if the target leaves the range but is within follow distance, he moves toward player) for the unit with this script
        if (gameObject.transform.parent.tag == "Player")
        {
            parentObject = gameObject.transform.parent.gameObject;
            parentObjectPlayerManager = parentObject.GetComponent<PlayerManager>();
            teamNumber = GetComponentInParent<TeamNumber>().teamNumber;
            followDistance = range;
            partToRotate = GetComponentInParent<PlayerManager>().partToRotate;
            partToRotateChildBegRot = GetComponentInParent<PlayerManager>().partToRotateChildBegRot;
            if (isPrimaryWeapon == true)
            {
                firepoint = GetComponentInParent<PlayerManager>().firepoint;
            }
            else
            {
                firepoint = this.transform;
            }
            photonView = GetComponentInParent<PhotonView>();
        }
        else
        {
            parentObject = gameObject.transform.parent.gameObject;
            parentObjectUnitManager = parentObject.GetComponent<UnitManager>();
            teamNumber = GetComponentInParent<TeamNumber>().teamNumber;
            movementSpeed = GetComponentInParent<UnitManager>().movementSpeed;
            followDistance = GetComponentInParent<UnitManager>().followDistance;            
            IsMovingUnit = gameObject.GetComponentInParent<UnitManager>().IsMovingUnit;
            partToRotate = GetComponentInParent<UnitManager>().partToRotate;
            partToRotateChildBegRot = GetComponentInParent<UnitManager>().partToRotateChildBegRot;
            if (isPrimaryWeapon == true)
            {
                firepoint = GetComponentInParent<UnitManager>().firepoint;
            }
            else
            {
                firepoint = this.transform;
            }
            if (IsMovingUnit == true)
            {
                agent = gameObject.GetComponentInParent<NavMeshAgent>();
            }
            StartDamageUnit(parentObjectUnitManager.unitLevel);
            photonView = GetComponentInParent<PhotonView>();

        }
        // If there is a part to rotate, set the beginning rotation that it will return to
        if (partToRotate != null)
        {
            partToRotateChildBegRot.transform.localRotation = partToRotate.transform.localRotation;
        }
        // Begin running update target every 0.33 seconds
        InvokeRepeating("UpdateTarget", 0f, 0.33f);





    }

    void StartDamageUnit(int level)
    {
        for (int i = 1; i <= level; i++)
        {
            damage = Mathf.RoundToInt((damage * (1 + ((float)i / 60))));
        }


    }

    public void UpdateTeamNumber()
    {
        teamNumber = GetComponentInParent<TeamNumber>().teamNumber;
    }

    public void UpdateTarget()
    {
        // Makes sure that this unit has a team number set
        if (teamNumber == 0)
        {
            teamNumber = GetComponentInParent<TeamNumber>().teamNumber;            
        }
        // Only updates the target if there is no current target and the current target is outside follow distance
        if (target == null)
        {
            //Cycles through all gameobject with tag enemies
            //If a new enemy is closer than a previous and within range, it sets them as nearest enemy
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");                          // Creates empty array of units tagged with "Enemy"
            float shortestDistanceToEnemy = 50;                                                         // Only sets enemies within 50 as nearest enemy
            GameObject nearestEnemy = null;                                                             // Makes sure that the nearest enemy is reset each time


            foreach (GameObject enemy in enemies)
            {

                {
                    //  finds the distance to the enemy, and if the enemy is within follow distance and closer than any previously set enemy, set this enemy as the nearest enemy
                    float distanceToEnemy = Vector3.Distance(gameObject.transform.parent.transform.position, enemy.transform.position);
                    if ((distanceToEnemy < shortestDistanceToEnemy) && enemy != this.gameObject.transform.parent && distanceToEnemy <= followDistance)
                    {
                        if (enemy.GetComponent<TeamNumber>().teamNumber != teamNumber) // makes sure that nearest enemy is not on same team as this object
                        {
                            shortestDistanceToEnemy = distanceToEnemy;
                            nearestEnemy = enemy;
                        }


                    }
                }

            }
            //Cycles through all gameobject with tag Player
            //If a new player is closer than a previous and within range, it sets them as nearest player
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");                             // Empty array of gameobjects with the tag of player
            float shortestDistanceToPlayer = 50;                                                            // Only sets players within 50 as nearest player
            GameObject nearestPlayer = null;                                                                // Makes sure that the nearest player is reset each time

            foreach (GameObject player in players)
            {

                {
                    // Finds teh distance to the player, and if the player is within follow distance and clsoer than any previously set player, set this player as nearest player
                    float distanceToEnemy = Vector3.Distance(gameObject.transform.parent.transform.position, player.transform.position);
                    if ((distanceToEnemy < shortestDistanceToPlayer) && player != this.gameObject.transform.parent && distanceToEnemy <= followDistance)
                    {
                        if (player.GetComponent<TeamNumber>().teamNumber != teamNumber) // makes sure that nearest player is not on same team as this object
                        {
                            shortestDistanceToPlayer = distanceToEnemy;
                            nearestPlayer = player;

                        }


                    }
                }

            }

            //Sets the nearest target to the gameobject that is closer between enemy unit or player.
            float shortestDistance;
            if (nearestPlayer != null && nearestEnemy != null)  // If there is both a player and enemy within range
            {
                // If the nearest player is closer than the nearest enemy, set nearest player as nearest target
                if (Vector3.Distance(nearestPlayer.transform.position, gameObject.transform.parent.transform.position) < Vector3.Distance(nearestEnemy.transform.position, gameObject.transform.parent.transform.position)) 
                {
                    nearestTarget = nearestPlayer;
                    shortestDistance = shortestDistanceToPlayer;
                }
                else // Nearest enemy is the nearest target
                {
                    nearestTarget = nearestEnemy;
                    shortestDistance = shortestDistanceToEnemy;
                }
            }
            else if (nearestPlayer != null) // If there is a nearest player, set the nearest player as nearest target
            {
                nearestTarget = nearestPlayer;
                shortestDistance = shortestDistanceToPlayer;
            }
            else // If there is no nearest player, then the nearest enemy becomes the nearest target
            {
                nearestTarget = nearestEnemy;
                shortestDistance = shortestDistanceToEnemy;
            }


            //If there is a nearest target and the shortest distance between both players and enemies is less than range or follow distance, target is set.
            //Else tells a regular unit that there is no target so they can continue on waypoints
            //Tells non-player to look at the target
            if (nearestTarget != null && shortestDistance <= followDistance)
            {
                target = nearestTarget.transform;

                if (this.gameObject.transform.parent.tag != "Player") // If the gameojbect is not a player, set the look target, stop the waypoint system, and tell the unit manager that a target has been set
                {
                    lookTarget = true;
                    if (GetComponentInParent<UnitManager>() != null)
                    {
                        if (agent != null)
                        {
                            agent.isStopped = true;
                        }

                        GetComponentInParent<UnitManager>().hasTarget = true;
                        

                    }
                }
            }
            else // If there is not a nearest target and the distance is not within follow distance, set target to null, look target to null, start the waypoint system again, and tell the unit manager there is no target
            {

                target = null;
                lookTarget = false;
                if (GetComponentInParent<UnitManager>() != null)
                {

                    GetComponentInParent<UnitManager>().hasTarget = false;
                    if (agent != null)
                    {
                        agent.isStopped = false;
                    }
                    
                    

                }

            }
        }
        

    }
	
	// Update is called once per frame
	public void Update ()
    {
        //If there is no target, you don't need to rotate a part toward a target, look at a target, move toward a target, or shoot at a target, so return
        if (target == null)
        {
            // If there is no target we want to reset our part to rotate's rotation back to the original rotation
            if (partToRotate != null)
            {
                RotateToBeginningRotation();
            }
            return;
        }

        //target lock on, if there is a part to rotate
        if (partToRotate != null)
        {
            RotateToTarget();
        }
        //Finds distance to target
        targetDistance = Vector3.Distance(gameObject.transform.parent.transform.position, target.transform.position);

        // These functions rotate the unit around and move the unit to the target, so we only want to do this if the unit is not the player
        if (this.gameObject.transform.parent.tag != "Player")
        {
            if (targetDistance < range)
            {
                if (parentObjectUnitManager.anim != null)
                {
                    parentObjectUnitManager.anim.SetBool("isRunning", false);
                    parentObjectUnitManager.anim.SetBool("isShooting", true);
                }
                
            }
            //If this is a unit and he has a target, need to look at target
            if (lookTarget == true)
            {
                if (lookY == true)
                {
                    lookYTarget = target.position.y;
                }
                else
                {
                    lookYTarget = gameObject.transform.parent.transform.position.y;
                }
                Vector3 targetLookAt = new Vector3(target.position.x, lookYTarget, target.transform.position.z); // Finds the vector 3 of the target
                Vector3 pos = targetLookAt - gameObject.transform.parent.position; // Finds the direction of the target
                var newRot = Quaternion.LookRotation(pos); // set the quaternion rotation of the direction of the target
                if (targetDistance < range)
                {
                    float diff = gameObject.transform.parent.transform.rotation.eulerAngles.y - firepoint.rotation.eulerAngles.y;
                    newRot *= Quaternion.Euler(0, diff, 0);
                }
                

                if (IsMovingUnit == true)
                {
                    gameObject.transform.parent.transform.rotation = Quaternion.Lerp(gameObject.transform.parent.transform.rotation, newRot, Time.deltaTime * 15); // lerp rotate towards the look target
                    //gameObject.transform.parent.transform.rotation = Quaternion.Lerp(firepoint.localRotation, newRot, Time.deltaTime * 15);
                }


            }

            //If the unit has a target and look target is true, but the target is outside of his range, he needs to move toward the target            
            if ((targetDistance > range) && lookTarget == true && IsMovingUnit == true)
            {
                if (parentObjectUnitManager.anim != null)
                {
                    parentObjectUnitManager.anim.SetBool("isRunning", true);
                    parentObjectUnitManager.anim.SetBool("isShooting", false);
                }
                MoveToTarget();
                
                

            }
            
            // If the target distance is greater than follow distance, reset everything and allow unit to move on waypoint system again.
            if (targetDistance > followDistance)
            {
                target = null;
                GetComponentInParent<UnitManager>().hasTarget = false;
                lookTarget = false;
                if (agent != null)
                {
                    agent.isStopped = false;
                }

            }
        }
        // If the unit is the player, and the target distance is longer than the range, set target to null
        else if (this.gameObject.transform.parent.tag == "Player")
        {
            if (targetDistance > range)
            {

                target = null;
            }
        }


        // If the fire cooldown is 0 and the target is within range, fire the bullet and set the cooldown
        if (fireCountdown <= 0f && targetDistance < range)
        {
            if (photonView != null)
            {
                photonView.RPC("PunShoot", PhotonTargets.All);
                fireCountdown = 1f / fireRate;
            }
            
        }
        // Increment the fire cooldown by seconds
        fireCountdown -= Time.deltaTime;
        

    }

    #region Functions
    public virtual void RotateToBeginningRotation()
    {
        if (isPrimaryWeapon == true)
        {
            // Create a vector 3 rotation by lerping towards the original rotation from our current rotation, then rotate to that rotation on only the y and z axis
            Vector3 rotation = Quaternion.Lerp(partToRotate.localRotation, partToRotateChildBegRot.transform.localRotation, Time.deltaTime * turnSpeed).eulerAngles;
            partToRotate.localRotation = Quaternion.Euler(0f, rotation.y, rotation.z);
        }
        
    }

    public virtual void RotateToTarget()
    {
        if (isPrimaryWeapon == true)
        {
            Vector3 dir = target.position - gameObject.transform.parent.transform.position;                                     // Finds the direction of the target
            Quaternion lookRotation = Quaternion.LookRotation(dir);                                                             // Looks in that direction
            Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;    // Create a vector 3 that lerps towards that look direction
            if (gameObject.transform.parent.transform.tag == "Player")
            {
                parentObjectPlayerManager.partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);                         // Actually rotate on only the y axis towards that rotation
            }
            else if (gameObject.transform.parent.transform.tag != "Player" && parentObjectUnitManager != null)
            {
                parentObjectUnitManager.partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);                           // Actually rotate on only the y axis towards that rotation
            }
        }
        
    }

    // Unit should already be looking toward target, so transform forward should move unit towards target
    public virtual void MoveToTarget()
    {
        gameObject.transform.parent.transform.position += transform.forward * Time.deltaTime * movementSpeed;
    }

    // Create the bullet object to shoot, then tell it to seek, giving it the target, the damage of this weapon, and who is shooting the weapon
    [PunRPC]
    public void PunShoot()
    {
        GameObject bulletGO = PhotonNetwork.Instantiate(bulletPrefabName, firepoint.position, firepoint.rotation,0);
        Bullet bullet = bulletGO.GetComponent<Bullet>();

        if (bullet != null)
        {
            bullet.Seek(target,damage,this.gameObject.transform.parent.gameObject, teamNumber, this.gameObject.transform.parent.gameObject.transform.tag);
        }
    }

    // Draw a gizmo in scene view of the range of this weapon
    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(gameObject.transform.position, range);
    }
    #endregion
}
