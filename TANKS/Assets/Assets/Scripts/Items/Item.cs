﻿using UnityEngine;

[CreateAssetMenu(fileName = "Item",menuName ="Inventory")]
public class Item : ScriptableObject
{
    new public string name = "New Item";        // Name of item
    public Sprite icon = null;                  // Icon of the item
    public bool isDefaultItem = false;          // Whether or not the item is a default item
    public bool isTankWeapon;                   // Whether or not the item is a weapon. used to know whether or not to change the team number of the weapon on pickup
    public GameObject itemPrefab;               // Prefab of the item that actually holds the script for the weapon or whatever the item is
    public GameObject itemPickupObject;         // Prefab that is the pickup object
    public string dictionaryString;

    // These are used to determine bounty. Easier to get reference if they are here as well as weapon script
    public float range;
    public float damage;
    public float fireRate;
    public float cost;
}
