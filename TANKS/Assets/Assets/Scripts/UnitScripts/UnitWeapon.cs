﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitWeapon : Photon.MonoBehaviour {

    [Header("Attributes")]
    public float range;
    public float fireRate;
    public float damage;
    private float fireCountdown;
    [Header("SetupFields")]
    public Transform Firepoint;
    public GameObject bulletPrefab;

    FindTarget findTarget;
    GameObject bulletGO;
    private string bulletPrefabName;
    private int teamNumber;


    private void Awake()
    {
        if (GetComponent<FindTarget>() != null)
        {
            findTarget = GetComponent<FindTarget>();
        }
        if (bulletPrefab != null)
        {
            bulletPrefabName = bulletPrefab.name;
        }
    }


    // Use this for initialization
    void Start ()
    {
        teamNumber = GetComponent<TeamNumber>().teamNumber;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (findTarget.target != null)
        {
            // If the fire cooldown is 0 and the target is within range, fire the bullet and set the cooldown
            float targetDistance = Vector3.Distance(findTarget.target.transform.position, transform.position);
            if (targetDistance < range && fireCountdown <= 0)
            {
                PhotonView.RPC("PunShoot", PhotonTargets.MasterClient);                
                fireCountdown = fireRate;
            }
            // Increment the fire cooldown by seconds
            fireCountdown -= Time.deltaTime;
        }
        
    }

    [PunRPC]
    private void PunShoot()
    {
        bulletGO = PhotonNetwork.InstantiateSceneObject(bulletPrefabName, Firepoint.position, Firepoint.rotation, 0, null);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        if (bullet != null)
        {
            bullet.Seek(findTarget.target.transform, damage, gameObject, teamNumber, gameObject.transform.tag);
        }

    }
}
