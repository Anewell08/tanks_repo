﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTankWeaponCannon : AbilityScript
{
    private PlayerManager parentObjectPlayerManager;
    private int teamNumber;
    private Transform target;
    private bool lookTarget;

    [Header("Attributes")]
    // Attributes of the weapon. Fire rate is measured in shots per second
    public float range = 18f;
    [Tooltip("Fire rate is measured in shots per second.")]
    public float damage = 80;

    [Header("UnitySetupFields")]

    public float turnSpeed = 10;                    // How quickly the part to rotate rotates
    // Bool to know whether or not it is the primary weapon in order to know whether the unit needs to rotate its part to rotate. We don't want to rotate if this is our second weapon.
    public bool isPrimaryWeapon = true;            // Also set to false as most weapons created will not be the primary weapon
    protected Transform partToRotate;               // The part that needs to rotate. Received from player or unit manager
    protected Transform partToRotateChildBegRot;    // Beginning rotation of the part to rotate. Received from player or unit manager
    Transform currentTransform;
    private GameObject nearestTarget;                // nearest player and nearest enemy are used to set this
    public GameObject bulletPrefab;                 // Bullet prefab to shoot
    protected Transform firepoint;                  // Point where the bullet will leave from
    protected float targetDistance;             // Float used to find the distance to the target
    public bool cannonMoved = false;
    private Vector3 storedPosition;
    private bool positionStored;

    public override void Start()
    {
        base.Start();
        abilityTextString = "The basic tank's primary weapon. Fires a shell from a cannon that deals " + Mathf.RoundToInt(damage * levelBoostPercentage) + "damage every 2 seconds. Has a range of 18";
        parentObjectPlayerManager = GetComponent<PlayerManager>();
        teamNumber = GetComponent<TeamNumber>().teamNumber;
        partToRotate = GetComponent<PlayerManager>().partToRotate;
        partToRotateChildBegRot = GetComponent<PlayerManager>().partToRotateChildBegRot;
        firepoint = GetComponent<PlayerManager>().firepoint;
        // If there is a part to rotate, set the beginning rotation that it will return to
        if (partToRotate != null)
        {
            partToRotateChildBegRot.transform.localRotation = partToRotate.transform.localRotation;
        }
        InvokeRepeating("UpdateTarget", 0f, 0.33f);
    }


    public override void UpdateLevel(int unitPlayerLevel)
    {
        base.UpdateLevel(unitPlayerLevel);
        abilityTextString = "The basic tank's primary weapon. Fires a shell from a cannon that deals " + Mathf.RoundToInt(damage * levelBoostPercentage) + "damage every 2 seconds. Has a range of 18";
    }

    public void UpdateTarget()
    {
        // Makes sure that this unit has a team number set
        if (teamNumber == 0)
        {
            teamNumber = GetComponent<TeamNumber>().teamNumber;
        }
        // Only updates the target if there is no current target and the current target is outside follow distance
        if (target == null)
        {
            //Cycles through all gameobject with tag enemies
            //If a new enemy is closer than a previous and within range, it sets them as nearest enemy
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");                          // Creates empty array of units tagged with "Enemy"
            float shortestDistanceToEnemy = 50;                                                         // Only sets enemies within 50 as nearest enemy
            GameObject nearestEnemy = null;                                                             // Makes sure that the nearest enemy is reset each time


            foreach (GameObject enemy in enemies)
            {

                {
                    //  finds the distance to the enemy, and if the enemy is within follow distance and closer than any previously set enemy, set this enemy as the nearest enemy
                    float distanceToEnemy = Vector3.Distance(gameObject.transform.position, enemy.transform.position);
                    if ((distanceToEnemy < shortestDistanceToEnemy) && enemy != this.gameObject.transform)
                    {
                        if (enemy.GetComponent<TeamNumber>().teamNumber != teamNumber) // makes sure that nearest enemy is not on same team as this object
                        {
                            shortestDistanceToEnemy = distanceToEnemy;
                            nearestEnemy = enemy;
                        }


                    }
                }

            }
            //Cycles through all gameobject with tag Player
            //If a new player is closer than a previous and within range, it sets them as nearest player
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");                             // Empty array of gameobjects with the tag of player
            float shortestDistanceToPlayer = 50;                                                            // Only sets players within 50 as nearest player
            GameObject nearestPlayer = null;                                                                // Makes sure that the nearest player is reset each time

            foreach (GameObject player in players)
            {

                {
                    // Finds teh distance to the player, and if the player is within follow distance and clsoer than any previously set player, set this player as nearest player
                    float distanceToEnemy = Vector3.Distance(gameObject.transform.position, player.transform.position);
                    if ((distanceToEnemy < shortestDistanceToPlayer) && player != this.gameObject.transform)
                    {
                        if (player.GetComponent<TeamNumber>().teamNumber != teamNumber) // makes sure that nearest player is not on same team as this object
                        {
                            shortestDistanceToPlayer = distanceToEnemy;
                            nearestPlayer = player;

                        }


                    }
                }

            }

            //Sets the nearest target to the gameobject that is closer between enemy unit or player.
            float shortestDistance;
            if (nearestPlayer != null && nearestEnemy != null)  // If there is both a player and enemy within range
            {
                // If the nearest player is closer than the nearest enemy, set nearest player as nearest target
                if (Vector3.Distance(nearestPlayer.transform.position, gameObject.transform.position) < Vector3.Distance(nearestEnemy.transform.position, gameObject.transform.position))
                {
                    nearestTarget = nearestPlayer;
                    shortestDistance = shortestDistanceToPlayer;
                }
                else // Nearest enemy is the nearest target
                {
                    nearestTarget = nearestEnemy;
                    shortestDistance = shortestDistanceToEnemy;
                }
            }
            else if (nearestPlayer != null) // If there is a nearest player, set the nearest player as nearest target
            {
                nearestTarget = nearestPlayer;
                shortestDistance = shortestDistanceToPlayer;
            }
            else // If there is no nearest player, then the nearest enemy becomes the nearest target
            {
                nearestTarget = nearestEnemy;
                shortestDistance = shortestDistanceToEnemy;
            }
            //If there is a nearest target and the shortest distance between both players and enemies is less than range or follow distance, target is set.
            //Else tells a regular unit that there is no target so they can continue on waypoints
            //Tells non-player to look at the target
            if (nearestTarget != null && shortestDistance < range)
            {
                target = nearestTarget.transform;
            }
            else // If there is not a nearest target and the distance is not within follow distance, set target to null, look target to null, start the waypoint system again, and tell the unit manager there is no target
            {
                target = null;
                lookTarget = false;
            }
        }


    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        //If there is no target, you don't need to rotate a part toward a target, look at a target, move toward a target, or shoot at a target, so return
        if (target == null)
        {
            // If there is no target we want to reset our part to rotate's rotation back to the original rotation
            if (partToRotate != null)
            {
                RotateToBeginningRotation();
            }
            return;
        }

        //target lock on, if there is a part to rotate
        if (partToRotate != null)
        {
            RotateToTarget();
        }
        //Finds distance to target
        targetDistance = Vector3.Distance(gameObject.transform.position, target.transform.position);
        // If the unit is the player, and the target distance is longer than the range, set target to null
        if (this.gameObject.transform.tag == "Player")
        {
            if (targetDistance > range)
            {

                target = null;
            }
        }


        // If the fire cooldown is 0 and the target is within range, fire the bullet and set the cooldown
        if (countdown <= 0f && targetDistance < range)
        {
            PunShoot();
            
            
            cannonMoved = true;
            countdown = 1f / fireRate;
        }
        
        if (cannonMoved == true)
        {
            if (positionStored == false)
            {
                storedPosition = partToRotate.position;
                positionStored = true;
            }

            partToRotate.position = Vector3.Lerp(partToRotate.transform.position, partToRotate.transform.position + -partToRotate.transform.forward, 10 * Time.deltaTime);
            if (Mathf.Abs(storedPosition.x - partToRotate.transform.position.x) > 0.2f || Mathf.Abs(partToRotate.transform.localPosition.y) > 0.2f)
            {
                cannonMoved = false;
            }
        }
        if (cannonMoved == false)
        {
            partToRotate.localPosition = Vector3.Lerp(partToRotate.localPosition, new Vector3(0f, 0, -1.3f), 10 * Time.deltaTime);
            positionStored = false;
        }


    }

    #region Functions
    public virtual void RotateToBeginningRotation()
    {
        if (isPrimaryWeapon == true)
        {
            // Create a vector 3 rotation by lerping towards the original rotation from our current rotation, then rotate to that rotation on only the y and z axis
            Vector3 rotation = Quaternion.Lerp(partToRotate.localRotation, partToRotateChildBegRot.transform.localRotation, Time.deltaTime * turnSpeed).eulerAngles;
            partToRotate.localRotation = Quaternion.Euler(0f, rotation.y, rotation.z);
        }

    }

    public virtual void RotateToTarget()
    {
        if (isPrimaryWeapon == true)
        {
            Vector3 dir = target.position - gameObject.transform.position;                                     // Finds the direction of the target
            Quaternion lookRotation = Quaternion.LookRotation(dir);                                                             // Looks in that direction
            Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;    // Create a vector 3 that lerps towards that look direction
            parentObjectPlayerManager.partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);                         // Actually rotate on only the y axis towards that rotation
        }

    }

    // Create the bullet object to shoot, then tell it to seek, giving it the target, the damage of this weapon, and who is shooting the weapon
    [PunRPC]
    public void PunShoot()
    {
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();

        if (bullet != null)
        {
            bullet.Seek(target, Mathf.RoundToInt(damage * levelBoostPercentage), this.gameObject, teamNumber, this.gameObject.transform.tag);
        }

        
    }

    // Draw a gizmo in scene view of the range of this weapon
    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(gameObject.transform.position, range);
    }
    #endregion



}
