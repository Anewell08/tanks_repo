﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopInteractable : Interactable
{
    public GameObject ShopUI;

    public override void RemoveItem()
    {
        // This probably needs to be moved to another script. This removes the item from the inventory
        if (interactingPlayer.GetComponent<PlayerController>().droppingItem == true)
        {
            Item item = interactingPlayer.GetComponent<PlayerController>().itemToBeDropped;
            interactingPlayer.GetComponent<PhotonView>().RPC("PunRemove", PhotonTargets.AllViaServer, item.dictionaryString);
            interactingPlayer.GetComponent<PlayerManager>().currency += Mathf.RoundToInt(item.cost * 0.67f);
            // Do I need to add something here to add functionality to allow this object to receive the item?
        }
    }

    public override void ChangeActive(bool setValue)
    {
        if (setValue == true)
        {
            ShopUI.SetActive(true);
        }
        else
        {
            ShopUI.SetActive(false);
        }


    }

}
