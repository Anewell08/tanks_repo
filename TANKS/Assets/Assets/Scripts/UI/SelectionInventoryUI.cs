﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionInventoryUI : MonoBehaviour {

    public Transform itemsParent;       // Items parent of the inventory slots
    public Image unitplayerIcon;
    public Text unitplayerHealthText;
    public Slider unitplayerHealthSlider;
    public Text unitDamageText;
    public Text unitFireRateText;

    public GameObject playerUISelection;
    public GameObject unitUISelection;

    [HideInInspector]
    public float unitplayerMaxHealth;
    [HideInInspector]
    public float unitplayerCurrentHealth;
    [HideInInspector]
    public float unitDamage;
    [HideInInspector]
    public float unitFireRate;
    [HideInInspector]
    public UnitHealth unitplayerUnitHealthScript;


    public PlayerManager _target;       // Used to reference the player the UI is attached to
    [HideInInspector]
    public GameObject _selectiontarget;

    InventorySlot[] slots;              // Array of the inventory slots

    bool unitHealthScriptSet = false;

    void Awake()
    {
        this.GetComponent<Transform>().SetParent(GameObject.Find("Canvas").GetComponent<Transform>(), false); // Moves this UI to the canvas


    }

    private void Start()
    {
        slots = itemsParent.GetComponentsInChildren<InventorySlot>();   // Sets slots array to be equal to all the inventory slots
    }

    // Stores reference to the player the UI is attached to.
    public void SetTarget(PlayerManager target)
    {
        if (target == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> PlayMakerManager target for PlayerUI.SetTarget.", this);
            return;
        }
        // Cache references for efficiency
        _target = target;
        if (slots == null)
        {
            slots = itemsParent.GetComponentsInChildren<InventorySlot>();   // Sets slots array to be equal to all the inventory slots
        }
    }

    public void SetSelection(GameObject selectionTarget)
    {
        _selectiontarget = selectionTarget;
        if (_selectiontarget.transform.tag == "Player")
        {
            playerUISelection.SetActive(true);
            unitUISelection.SetActive(false);
        }
        else
        {
            playerUISelection.SetActive(false);
            unitUISelection.SetActive(true);
        }
        UpdateUI();

    }

    // Loops through the slots, adding item to slot if the inventory has an item to put there. Else clears the slot. This is what moves the items to the front when an item is dropped
    public void UpdateUI()
    {
        if (_selectiontarget != null)
        {
            if (_selectiontarget.transform.tag == "Player" || _selectiontarget.transform.tag == "Enemy")
            {
                unitplayerUnitHealthScript = _selectiontarget.GetComponent<UnitHealth>();
                unitHealthScriptSet = true;
            }
            if (_selectiontarget.transform.tag == "Player")
            {
                Inventory selectionTargetInventory = _selectiontarget.GetComponent<Inventory>();
                for (int i = 0; i < slots.Length; i++)
                {
                    if (i < selectionTargetInventory.items.Count)
                    {
                        slots[i].AddItem(selectionTargetInventory.items[i]);
                    }
                    else
                    {
                        slots[i].ClearSlot();
                    }
                }
            }
        }
           

    }

    private void Update()
    {
        // Destroys the UI if the player dies.
        if (_target == null)
        {
            Destroy(this.gameObject);
            return;
        }
        if (_selectiontarget != null && unitHealthScriptSet == true)
        {
            unitplayerCurrentHealth = unitplayerUnitHealthScript.currentHealth;
            unitplayerMaxHealth = unitplayerUnitHealthScript.maxHealth;
            unitplayerHealthText.text = unitplayerCurrentHealth.ToString() + "/" + unitplayerMaxHealth;
            unitplayerHealthSlider.value = unitplayerCurrentHealth;
            unitplayerHealthSlider.maxValue = unitplayerMaxHealth;
            if (_selectiontarget.transform.tag == "Enemy")
            {
                if (_selectiontarget != null)
                {
                    if (_selectiontarget.GetComponentInChildren<TankWeapon>() != null)
                    {
                        unitDamageText.text = "Damage: " + _selectiontarget.GetComponentInChildren<TankWeapon>().damage.ToString();
                        unitFireRateText.text = "Fire rate: " + _selectiontarget.GetComponentInChildren<TankWeapon>().fireRate.ToString();
                    }
                }
                
                
            }
        }
        if (_selectiontarget == null)
        {
            unitHealthScriptSet = false;
            gameObject.SetActive(false);
        }
    }
}
