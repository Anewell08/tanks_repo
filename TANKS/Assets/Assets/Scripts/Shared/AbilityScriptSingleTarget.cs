﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityScriptSingleTarget : AbilityScript
{

    public float range;
    public GameObject bulletPrefab;
    public GameObject smokeLaunchEffect;


    private bool abilityPrepared;
    private bool inRange = false;
    private Transform fireTargetLocation;
    private bool unitSelected;
    protected Interactable interactable;
    private bool moveUnit;
    private bool needToLookAt;

    public override void OnLeft()
    {

        if (countdown <= 0 && unitSelected == false)
        {
            Debug.Log("Ability prepared to fire");
            abilityPrepared = true;
            inRange = false;
        }

    }

    public virtual void StartAbilityCoroutine()
    {
        StartCoroutine("LaunchAPRound");
    }

    public IEnumerator LaunchAPRound()
    {

        Debug.Log("Launch ability");
        yield return new WaitForEndOfFrame();


    }

    public override void Update()
    {
        base.Update();
        if (abilityPrepared == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                interactable = null;
                unitSelected = false;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;


                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    abilityPrepared = false;
                    return;

                }
                else
                {

                    // if the mouse cast a ray
                    if (Physics.Raycast(ray, out hit, 100))
                    {

                        //check if we hit an interactable collider
                        interactable = hit.collider.GetComponent<Interactable>();
                        // if we did, and it has a unit health, set that as selection
                        if (interactable != null)
                        {
                            Debug.Log("target set on unit");
                            unitSelected = true;
                            if (Vector3.Distance(interactable.transform.position, gameObject.transform.position) < range)
                            {
                                fireTargetLocation = interactable.transform;
                                inRange = true;
                            }
                            else if (Vector3.Distance(interactable.transform.position, gameObject.transform.position) > range)
                            {
                                fireTargetLocation = interactable.transform;
                                inRange = false;
                            }
                        }


                    }
                }
            }
            else if (Input.anyKeyDown)
            {
                abilityPrepared = false;
            }
            if (unitSelected == true)
            {
                fireTargetLocation = interactable.transform;
                //check if we are in range
                if (Vector3.Distance(fireTargetLocation.position, gameObject.transform.position) < range)
                {
                    inRange = true;
                }
                else if (Vector3.Distance(fireTargetLocation.position, gameObject.transform.position) > range)
                {
                    inRange = false;
                }
            }
            if (inRange == true && needToLookAt == true)
            {
                LookAtTarget();
                if (Vector3.Dot(transform.forward, (fireTargetLocation.position - transform.position).normalized) > 0.999f) // if we are looking at the unit
                {
                    StartAbilityCoroutine(); //Start the ability
                    abilityPrepared = false;
                    unitSelected = false;
                    inRange = false;
                }

            }
            else if (inRange == true && needToLookAt == false)
            {
                StartAbilityCoroutine();
                abilityPrepared = false;
                unitSelected = false;
                inRange = false;
            }
            else if (inRange == false && unitSelected == true)
            {
                MoveAndLookAtTarget();                
            }



        }
        else if (abilityPrepared == false)
        {
            unitSelected = false;
            inRange = false;
            moveUnit = false;
        }
    }

    public virtual void MoveAndLookAtTarget()
    {
        Vector3 targetLookAt = new Vector3(fireTargetLocation.position.x, gameObject.transform.position.y, fireTargetLocation.position.z); // Finds the vector 3 of the target
        Vector3 pos = targetLookAt - gameObject.transform.position; // Finds the direction of the target
        var newRot = Quaternion.LookRotation(pos); // set the quaternion rotation of the direction of the target
        gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, newRot, Time.deltaTime * 15); // lerp rotate towards the look target    
        moveUnit = true;
        if (Vector3.Distance(fireTargetLocation.position, gameObject.transform.position) < range)
        {
            inRange = true;
            moveUnit = false;
        }
    }

    public virtual void LookAtTarget()
    {
        Debug.Log("in range and location selected, ability getting ready to fire");
        Vector3 targetLookAt = new Vector3(fireTargetLocation.position.x, gameObject.transform.position.y, fireTargetLocation.position.z); // Finds the vector 3 of the target
        Vector3 pos = targetLookAt - gameObject.transform.position; // Finds the direction of the target
        var newRot = Quaternion.LookRotation(pos); // set the quaternion rotation of the direction of the target
        gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, newRot, Time.deltaTime * 10); // lerp rotate towards the look target  
        
    }

    private void FixedUpdate()
    {
        if (moveUnit == true)
        {
            transform.Translate(transform.forward * pManager.movementSpeed * Time.deltaTime, Space.World);
        }
    }
}
