﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyScript : MonoBehaviour
{
    private List<Item> itemList = new List<Item>(); // Reference to the current inventory

    public float GetCurrency(string damagerTag) // Returns how much the unit this script is attached to is worth
    {
        float currencyWorth = 0;                               // Resets how much it is worth
        if (gameObject.transform.tag == "Player")
        {
            itemList = gameObject.GetComponent<Inventory>().items; // Gets the current inventory
            
            foreach (Item weapon in itemList)                      // Runs through each item and adds an amount to currency worth based on the item characteristics
            {
                float damage = weapon.damage;
                float fireRate = weapon.fireRate;
                float range = weapon.range;

                float dps = damage / fireRate;

                currencyWorth += (dps * Random.Range(0.9f, 1.1f))  // DPS of the item multiplied by between 0.9 and 1.1
                    + ((range * 5) * Random.Range(0.9f, 1.1f));   // Range of the item multiplied by 5 multiplied by between 0.9 and 1.1

            }
            float health = gameObject.GetComponent<UnitHealth>().maxHealth; // Reference to the units max health
            currencyWorth += ((health / 10) * Random.Range(0.9f, 1.1f));    // Divides the units health by 10, multiplied by between 0.9 and 1.1, then adds to currency worth
            currencyWorth = (currencyWorth / 10);                           // Divides the currency worth by 10 to get a better number
            currencyWorth = Mathf.RoundToInt(currencyWorth);                // Rounds to nearest int
            if (damagerTag == "Player")
            {
                currencyWorth = currencyWorth * 2;
            }
            return currencyWorth;                                           // Gives the bounty
        }
        else
        {
            currencyWorth = (gameObject.GetComponent<UnitManager>().bounty * (1 + (gameObject.GetComponent<UnitManager>().unitLevel / 20))) * Random.Range(0.9f, 1.1f); // Increases currency by level and random amount;
            currencyWorth = Mathf.RoundToInt(currencyWorth);                // Rounds to nearest int
            if (damagerTag == "Player")
            {
                currencyWorth = currencyWorth * 2;
            }
        }
        return currencyWorth;                                           // Gives the bounty
    }
	
}
