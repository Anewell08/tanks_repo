﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPoint : MonoBehaviour
{

    private int teamNumber;                     // Team number set by child object
    private bool allyInRange;                   // Private bool telling whether there is an ally in range of this gameobject
    private bool enemyInRange;                  // Private bool telling whether there is an enemy in range of this gameobject
    private bool coroutineRunning = false;      // Private bool set to false on start that the coroutine is running
    private bool coroutinePaused = false;       // By default, this bool is set to false as the coroutine is not paused, only not running on start
    private float takeoverTimer;                // Timer that is incremented while an enemy unit is nearby


    public float takeOverTime = 5f;             // Time that the take over timer needs to reach to take over the control point
    public float healingRange;                  // Range of the healing and takeover of the control point



    private void Start()
    {
        // Find the team number of the control point
        if (this.gameObject.transform.Find("Team").gameObject.tag == "Team1")
        {
            teamNumber = 1;
        }
        else
        {
            teamNumber = 2;
        }
        // Invoke the find players function to run every 0.33 seconds.
        InvokeRepeating("FindPlayers", 0, 0.33f);
    }

    void FindPlayers()
    {
        // Create an array of players
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        // Reset who is in range
        allyInRange = false;
        enemyInRange = false;
        foreach (GameObject player in players)
        {
            // For each player in the game, find their distance, then if they are in range and the same team, heal them, and set ally in range equal to true so we can make sure this point can't be taken over
            float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
            if ((distanceToPlayer < healingRange) && player.GetComponent<TeamNumber>().teamNumber == teamNumber)
            {
                player.GetComponent<UnitHealth>().ControlPointHeal();
                allyInRange = true;
                Debug.Log("Ally in range set to true");

            }
            // If an enemy is in range, set enemy in range equal to true so we can know whether or not to begin capturing point
            if ((distanceToPlayer < healingRange) && player.GetComponent<TeamNumber>().teamNumber != teamNumber)
            {
                enemyInRange = true;
                Debug.Log("Enemy in range set to true");
            }
            

        }
        // If there is an enemy and no ally, and the point isn't currently being taken over, start the takeover
        if (enemyInRange == true && allyInRange == false && coroutineRunning == false)
        {
            StartCoroutine("ControlPointTakeover");
        }
        // If an ally enters while an enemy enters, make sure coroutine is paused
        if (allyInRange == true && enemyInRange == true)
        {
            coroutinePaused = true;
        }
        // If there is an enemy in range and no ally, and the coroutine is running, make sure it is unpaused
        if (enemyInRange == true && allyInRange == false && coroutineRunning == true)
        {
            coroutinePaused = false;
        }
        // If an enemy leaves the area, end the coroutine and set the timer to 0.
        if (enemyInRange == false)
        {
            coroutineRunning = false;
            takeoverTimer = 0;
        }


    }

    IEnumerator ControlPointTakeover()
    {
        Debug.Log("Coroutine began");
        coroutineRunning = true;                                                    // Make sure the coroutine knows it is running
        while (coroutineRunning == true)                                            // While the courtine is running, only run it every 0.1f seconds (see wait for seconds at bottom of while statement)
        {
            Debug.Log("Take Over timer: " + takeoverTimer);
            while (coroutinePaused == false && coroutineRunning == true)            // While the coroutine is running and unpaused, increment take over timer by 0.1f every 0.1f seconds
            {
                takeoverTimer += 0.1f;                                              
                Debug.Log("Take Over Timer: " + takeoverTimer);
                if (takeoverTimer >= takeOverTime)                                  // If the timer reaches the time needed to take over the point, change which team the control point belongs to
                {
                    coroutineRunning = false;                                       // End the coroutine after executing last functions
                    if (teamNumber == 1)                                            // Change the team number tag of the child object, update the color scheme, and change the teamNumber in this script, and reset take over timer
                    {
                        gameObject.transform.Find("Team").gameObject.tag = "Team2";
                        UpdateColorScheme(Color.red);
                        teamNumber = 2;
                        takeoverTimer = 0;
                    }
                    else
                    {
                        gameObject.transform.Find("Team").gameObject.tag = "Team1";
                        UpdateColorScheme(Color.blue);
                        teamNumber = 1;
                        takeoverTimer = 0;
                    }
                    yield return new WaitForSeconds(0.1f);
                }
                else
                {
                    yield return new WaitForSeconds(0.1f);
                }
            }
            yield return new WaitForSeconds(0.1f);                                  // here is where we wait for seconds if the coroutine is running but paused

        }
        coroutineRunning = false;                                                   // If the coroutine is no longer running, set it to false
        Debug.Log("Coroutine over, takeovertimer set to 0");
        takeoverTimer = 0;                                                          // If the coroutine is no longer running, make sure timer is set back to 0

    }

    public void UpdateColorScheme(Color color)
    {
        this.gameObject.GetComponent<MeshRenderer>().material.color = color;        // update the color to whatever color is passed through into this function
    }
}
