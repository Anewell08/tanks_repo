﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperienceScript : MonoBehaviour
{
    private List<Item> itemList = new List<Item>(); // Reference to the current inventory

    public float GetExperience(string damagerTag) // Returns how much the unit this script is attached to is worth
    {
        float experienceWorth = 0;                               // Resets how much it is worth
        if (gameObject.transform.tag == "Player")
        {
            itemList = gameObject.GetComponent<Inventory>().items; // Gets the current inventory

            foreach (Item weapon in itemList)                      // Runs through each item and adds an amount to experience worth based on the item characteristics
            {
                float damage = weapon.damage;
                float fireRate = weapon.fireRate;
                float range = weapon.range;

                float dps = damage / fireRate;

                experienceWorth += (dps * Random.Range(0.9f, 1.1f))  // DPS of the item multiplied by between 0.9 and 1.1
                    + ((range * 5) * Random.Range(0.9f, 1.1f));   // Range of the item multiplied by 5 multiplied by between 0.9 and 1.1

            }
            float health = gameObject.GetComponent<UnitHealth>().maxHealth; // Reference to the units max health
            experienceWorth += ((health / 10) * Random.Range(0.9f, 1.1f));    // Divides the units health by 10, multiplied by between 0.9 and 1.1, then adds to experience worth
            experienceWorth = experienceWorth * (1 + (gameObject.GetComponent<PlayerManager>().playerLevel / 20)); // Increases experience by level
            experienceWorth = (experienceWorth / 10);                           // Divides the experience worth by 10 to get a better number
            experienceWorth = Mathf.RoundToInt(experienceWorth);                // Rounds to nearest int
            if (damagerTag == "Player")
            {
                experienceWorth = experienceWorth * 2;
            }


            return experienceWorth;                                           // Gives the experience
        }
        else
        {
            experienceWorth = (gameObject.GetComponent<UnitManager>().bounty * (1 + (gameObject.GetComponent<UnitManager>().unitLevel / 20))) * Random.Range(0.9f, 1.1f); // Increases experience by level
            experienceWorth = Mathf.RoundToInt(experienceWorth);                // Rounds to nearest int
            if (damagerTag == "Player")
            {
                experienceWorth = experienceWorth * 2;
            }

        }
        return experienceWorth;                                           // Gives the experience
    }

}
