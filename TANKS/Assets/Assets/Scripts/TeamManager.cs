﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamManager : MonoBehaviour
{
    static public TeamManager Instance;

    // Text output of current experience
    public Text experience1Text;            
    public Text experience2Text;
    // Text output of current level
    public Text team1LevelText;
    public Text team2LevelText;
    // Slider for how close team is to next level
    public Slider experience1Slider;
    public Slider experience2Slider;

    // Variables for current and max experience, as well as current level
    [HideInInspector]
    public float team1Experience;
    [HideInInspector]
    public float team2Experience;
    [HideInInspector]
    public float maxExperienceTeam1;
    [HideInInspector]
    public float maxExperienceTeam2;
    [HideInInspector]
    public int currentLevelTeam1 = 1;
    [HideInInspector]
    public int currentLevelTeam2 = 1;

    private void Awake()
    {
        Instance = this;
    }

    public void Start()
    {
        // On start, set level to 1, display it in text
        currentLevelTeam1 = 1;
        currentLevelTeam2 = 1;
        team1LevelText.text = currentLevelTeam1.ToString();
        team2LevelText.text = currentLevelTeam2.ToString();
        // Set max experience for first level to 500, set slider max value
        maxExperienceTeam1 = 100;
        maxExperienceTeam2 = 100;
        experience1Slider.maxValue = maxExperienceTeam1;
        experience2Slider.maxValue = maxExperienceTeam2;
    }

    // Called from unit health, update experience of the team. Takes a team number and amount of experience to add (comes from experience script)
    public void UpdateExperience(int teamNumber, float experience)
    {
        if (teamNumber == 1)
        {
            // Add the experience
            team1Experience += experience;
            // if experience reaches max, increase level, change text, level up units, reset experience back down to 0 (or close to it), set a new max experience, set a new max value
            while (team1Experience >= maxExperienceTeam1)
            {
                currentLevelTeam1++;
                team1LevelText.text = currentLevelTeam1.ToString();
                LevelUp(1);
                team1Experience -= maxExperienceTeam1;
                maxExperienceTeam1 = maxExperienceTeam1 * 2;
                experience1Slider.maxValue = maxExperienceTeam1;
            }
        }
        else if (teamNumber == 2)
        {
            team2Experience += experience;
            while (team2Experience >= maxExperienceTeam2)
            {
                currentLevelTeam2++;
                team2LevelText.text = currentLevelTeam2.ToString();
                LevelUp(2);
                team2Experience -= maxExperienceTeam2;
                maxExperienceTeam2 = maxExperienceTeam2 * 2;
                experience2Slider.maxValue = maxExperienceTeam2;
            }
        }

    }

    //Levels up players and units
    public void LevelUp(int teamNumber)
    {
        // Call level up on units, players
        // Empty array of gameobjects with the tag of player
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");                            

        foreach (GameObject player in players)
        {
            int playerTeamNumber = player.GetComponent<TeamNumber>().teamNumber;
            if (teamNumber == playerTeamNumber)
            {
                int teamLevel;
                if (playerTeamNumber == 1)
                {
                    teamLevel = currentLevelTeam1;
                }
                else
                {
                    teamLevel = currentLevelTeam2;
                }
                player.GetComponent<PlayerManager>().LevelUp(teamLevel);
            }
            
            
        }

        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");                          // Creates empty array of units tagged with "Enemy"

        foreach (GameObject enemy in enemies)
        {
            int unitTeamNumber = enemy.GetComponent<TeamNumber>().teamNumber;
            if (teamNumber == unitTeamNumber)
            {
                int teamLevel;
                if (unitTeamNumber == 1)
                {
                    teamLevel = currentLevelTeam1;
                }
                else
                {
                    teamLevel = currentLevelTeam2;
                }
                enemy.GetComponent<UnitManager>().LevelUp(teamLevel);
            }
        }
        
        foreach (GameObject player in players)
        {
            if (player.GetComponent<PlayerManager>().selectionUIScript != null)
            {
                player.GetComponent<PlayerManager>().selectionUIScript.UpdateUI();
            }
            
        }

    }

    // Constantly update slider and text for current experience
    public void Update()
    {
        experience1Slider.value = team1Experience;
        experience2Slider.value = team2Experience;
        experience1Text.text = team1Experience.ToString() + "/" + maxExperienceTeam1.ToString();
        experience2Text.text = team2Experience.ToString() + "/" + maxExperienceTeam2.ToString();
    }

}
