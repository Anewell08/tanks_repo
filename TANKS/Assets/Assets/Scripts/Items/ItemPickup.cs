﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : Interactable
{

    public Item item;

    private Inventory playerInv;
    private bool wasPickedUp;
    private PhotonView playerPV;

    // Runs the base interact then pickup item
    public override void Interact()
    {
        base.Interact();
        playerPV = interactingPlayer.GetComponent<PhotonView>();
        playerInv = interactingPlayer.GetComponent<PlayerManager>().Inventory;
        if (playerPV != null && playerPV.isMine)
        {
            PhotonView.RPC("PunPickUp", PhotonTargets.AllViaServer);
        }
        
    }

    // Adds the item to the inventory then the bool returns true if the item was added. If the item was a tank weapon, update the team number, then destroy this item pickup prefab
    [PunRPC]
    public void PunPickUp()
    {
        playerPV.RPC("PunAdd", PhotonTargets.AllViaServer, item.dictionaryString,PhotonView.viewID);
    }
}
