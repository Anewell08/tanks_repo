﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class UnitSpawner : MonoBehaviour
{
    public static UnitSpawner instance;


    public GameObject[] leftTopPoints;
    public GameObject[] rightTopPoints;
    public GameObject[] leftMiddlePoints;
    public GameObject[] rightMiddlePoints;
    public GameObject[] leftBottomPoints;
    public GameObject[] rightBottomPoints;
    public GameObject Creep;
    public GameObject leftUnitSpawnLocation;
    public GameObject rightUnitSpawnLocation;

    public PhotonView photonView;

    float timer;
    float countdown;
    private bool masterClientTrue = false;

    private void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start ()
    {
        if (PhotonNetwork.isMasterClient)
        {
            masterClientTrue = true;
            timer = 0;
        }
    }


    public void SpawnUnits()
    {
        GameObject creepLT = PhotonNetwork.InstantiateSceneObject("TestAgent", leftUnitSpawnLocation.transform.position + new Vector3(Random.Range(0f, 5f), 0, Random.Range(0f, 5f)), Quaternion.identity, 0, null) as GameObject;
        for (int i = 0; i < leftTopPoints.Length; i++)
        {
            creepLT.GetComponent<WayPointNavigator>().points[i] = leftTopPoints[i].transform.position;
        }
        
        creepLT.GetComponent<TeamNumber>().teamNumber = 1;

        //GameObject creepLM = PhotonNetwork.InstantiateSceneObject("TestAgent", leftUnitSpawnLocation.transform.position + new Vector3(Random.Range(0f, 5f), 0, Random.Range(0f, 5f)), Quaternion.identity, 0, null) as GameObject;
        //creepLM.GetComponent<WayPointNavigator>().points = leftMiddlePoints;
        //creepLM.GetComponent<TeamNumber>().teamNumber = 1;

        //GameObject creepLB = PhotonNetwork.InstantiateSceneObject("TestAgent", leftUnitSpawnLocation.transform.position + new Vector3(Random.Range(0f, 5f), 0, Random.Range(0f, 5f)), Quaternion.identity, 0, null) as GameObject;
        //creepLB.GetComponent<WayPointNavigator>().points = leftBottomPoints;
        //creepLB.GetComponent<TeamNumber>().teamNumber = 1;

        //GameObject creepRT = PhotonNetwork.InstantiateSceneObject("TestAgent", rightUnitSpawnLocation.transform.position + new Vector3(Random.Range(0f, 5f), 0, Random.Range(0f, 5f)), Quaternion.identity, 0, null) as GameObject;
        //creepRT.GetComponent<WayPointNavigator>().points = rightTopPoints;
        //creepRT.GetComponent<TeamNumber>().teamNumber = 2;

        //GameObject creepRM = PhotonNetwork.InstantiateSceneObject("TestAgent", rightUnitSpawnLocation.transform.position + new Vector3(Random.Range(0f, 5f), 0, Random.Range(0f, 5f)), Quaternion.identity, 0, null) as GameObject;
        //creepRM.GetComponent<WayPointNavigator>().points = rightMiddlePoints;
        //creepRM.GetComponent<TeamNumber>().teamNumber = 2;

        //GameObject creepRB = PhotonNetwork.InstantiateSceneObject("TestAgent", rightUnitSpawnLocation.transform.position + new Vector3(Random.Range(0f, 5f), 0, Random.Range(0f, 5f)), Quaternion.identity, 0, null) as GameObject;
        //creepRB.GetComponent<WayPointNavigator>().points = rightBottomPoints;
        //creepRB.GetComponent<TeamNumber>().teamNumber = 2;
    }


    private void Update()
    {
        if (masterClientTrue == true)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                SpawnUnits();
                //SpawnUnits();
                //SpawnUnits();
                //SpawnUnits();

                timer = 15;
            }
        }




    }


}
