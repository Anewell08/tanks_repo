﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTankAPRound : AbilityScript {

    public float range;
    public GameObject bulletPrefab;
    public GameObject smokeLaunchEffect;


    private bool RocketPreparedToFire;
    private bool inRange = false;
    private Transform fireTargetLocation;
    private bool unitSelected;
    Interactable interactable;
    private bool moveUnit;


    public override void Start()
    {
        base.Start();
        abilityTextString = "Launch an armor piercing round that ignores armor and deals " + Mathf.Round(200f * levelBoostPercentage) + " damage to a single unit.";
    }

    public override void OnLeft()
    {

        if (countdown <= 0 && unitSelected == false)
        {
            Debug.Log("AP round prepared to fire");
            RocketPreparedToFire = true;
            inRange = false;
        }

    }

    public override void UpdateLevel(int unitPlayerLevel)
    {
        base.UpdateLevel(unitPlayerLevel);
        abilityTextString = "Launch an armor piercing round that ignores armor and deals " + Mathf.Round(200f * levelBoostPercentage) + " damage to a single unit.";
    }

    [PunRPC]
    public void PunStartLaunchRound()
    {
        StartCoroutine("LaunchAPRound");
    }


    public IEnumerator LaunchAPRound()
    {
        //launch bullet
        GameObject bulletGO = PhotonNetwork.Instantiate("BasicTankAPRoundBulletPrefab", pManager.firepoint.position, pManager.firepoint.rotation,0);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        if (bullet != null)
        {
            bullet.Seek(fireTargetLocation, Mathf.Round(200f * levelBoostPercentage), this.gameObject, gameObject.GetComponent<TeamNumber>().teamNumber, this.gameObject.transform.tag);
        }
        GameObject effectIns = PhotonNetwork.Instantiate("BasicTankRocketSmokeEffect", pManager.firepoint.position, pManager.firepoint.rotation,0);
        effectIns.transform.rotation *= Quaternion.Euler(-90, 0, 0);
        effectIns.transform.parent = pManager.firepoint.transform;
        

        countdown = 1f / fireRate;
        //create smoke effect
        //move cannon backward
        //reset countdown
        //wait for seconds
        //move cannon forward
        Debug.Log("Launch AP round");
        yield return new WaitForSeconds(5);
        PhotonNetwork.Destroy(effectIns);

    }

    public override void Update()
    {
        base.Update();
        if (RocketPreparedToFire == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                interactable = null;
                unitSelected = false;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;


                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    RocketPreparedToFire = false;
                    return;

                }
                else
                {

                    // if the mouse cast a ray
                    if (Physics.Raycast(ray, out hit, 100))
                    {

                        //check if we hit an interactable collider
                        interactable = hit.collider.GetComponent<Interactable>();
                        // if we did, and it has a unit health, set that as selection
                        if (interactable != null)
                        {
                            Debug.Log("target set on unit");
                            unitSelected = true;
                            if (Vector3.Distance(interactable.transform.position, gameObject.transform.position) < range)
                            {
                                fireTargetLocation = interactable.transform;
                                inRange = true;
                            }
                            else if (Vector3.Distance(interactable.transform.position, gameObject.transform.position) > range)
                            {
                                fireTargetLocation = interactable.transform;
                                inRange = false;
                            }
                        }
                        

                    }
                }
            }
            else if (Input.anyKeyDown)
            {
                RocketPreparedToFire = false;
            }
            if (unitSelected == true)
            {
                fireTargetLocation = interactable.transform;
                if (Vector3.Distance(fireTargetLocation.position, gameObject.transform.position) < range)
                {
                    inRange = true;
                }
                else if (Vector3.Distance(fireTargetLocation.position, gameObject.transform.position) > range)
                {
                    inRange = false;
                }
            }
            if (inRange == true)
            {
                Debug.Log("in range and location selected, AP round getting ready to fire");
                Vector3 targetLookAt = new Vector3(fireTargetLocation.position.x, gameObject.transform.position.y, fireTargetLocation.position.z); // Finds the vector 3 of the target
                Vector3 pos = targetLookAt - gameObject.transform.position; // Finds the direction of the target
                var newRot = Quaternion.LookRotation(pos); // set the quaternion rotation of the direction of the target
                gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, newRot, Time.deltaTime * 10); // lerp rotate towards the look target  
                if (Vector3.Dot(transform.forward, (fireTargetLocation.position - transform.position).normalized) > 0.999f) // if we are looking at the unit
                {
                    playerPhotonView.RPC("PunStartLaunchRound", PhotonTargets.AllBufferedViaServer);
                    RocketPreparedToFire = false;
                    unitSelected = false;
                    inRange = false;
                }

            }
            else if (inRange == false && unitSelected == true)
            {
                Vector3 targetLookAt = new Vector3(fireTargetLocation.position.x, gameObject.transform.position.y, fireTargetLocation.position.z); // Finds the vector 3 of the target
                Vector3 pos = targetLookAt - gameObject.transform.position; // Finds the direction of the target
                var newRot = Quaternion.LookRotation(pos); // set the quaternion rotation of the direction of the target
                gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, newRot, Time.deltaTime * 15); // lerp rotate towards the look target    
                moveUnit = true;
                if (Vector3.Distance(fireTargetLocation.position, gameObject.transform.position) < range)
                {
                    inRange = true;
                    moveUnit = false;
                }
            }
            


        }
        else if (RocketPreparedToFire == false)
        {
            unitSelected = false;
            inRange = false;
            moveUnit = false;
        }
    }

    private void FixedUpdate()
    {
        if (moveUnit == true)
        {
            transform.Translate(transform.forward * pManager.movementSpeed * Time.deltaTime, Space.World);
        }
    }
}

