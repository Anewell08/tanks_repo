﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
public class ClickableButton : MonoBehaviour, IPointerClickHandler
{
    //Events for button clicks
    public UnityEvent onLeft;
    public UnityEvent onRight;
    public UnityEvent onMiddle;





    // Sets up the button to use right click and middle click
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            onLeft.Invoke();
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            onRight.Invoke();
        }
        else if (eventData.button == PointerEventData.InputButton.Middle)
        {
            onMiddle.Invoke();
        }
    }


}
