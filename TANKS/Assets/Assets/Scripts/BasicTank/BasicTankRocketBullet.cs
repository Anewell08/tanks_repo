﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTankRocketBullet : Bullet
{


    /// <summary>
    /// Called when bullet reaches the target.
    /// </summary>
    public override void HitTarget()
    {
        // If there is not target, destroy the bullet and don't do anything else.
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        // Create the impact effect, then destroy it after two seconds.
        GameObject effectIns = (GameObject)Instantiate(ImpactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 5);


        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");                          // Creates empty array of units tagged with "Enemy"

        foreach (GameObject enemy in enemies)
        {

            {
                //  finds the distance to the enemy, and if the enemy is within follow distance and closer than any previously set enemy, set this enemy as the nearest enemy
                float distanceToEnemy = Vector3.Distance(gameObject.transform.position, enemy.transform.position);
                if ((distanceToEnemy < explosionRange) && enemy != this.gameObject)
                {
                    if (enemy.GetComponent<TeamNumber>().teamNumber != teamNumberShooter) // makes sure that nearest enemy is not on same team as this object
                    {
                        // Damage the target by the determined amount
                        enemy.GetComponent<UnitHealth>().DamageTarget(damage, weaponShooter, teamNumberShooter, damagerTag);
                    }


                }
            }

        }
        //Cycles through all gameobject with tag Player
        //If a new player is closer than a previous and within range, it sets them as nearest player
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");                             // Empty array of gameobjects with the tag of player

        foreach (GameObject player in players)
        {

            {
                // Finds teh distance to the player, and if the player is within follow distance and clsoer than any previously set player, set this player as nearest player
                float distanceToEnemy = Vector3.Distance(gameObject.transform.position, player.transform.position);
                if ((distanceToEnemy < explosionRange) && player != this.gameObject)
                {
                    if (player.GetComponent<TeamNumber>().teamNumber != teamNumberShooter) // makes sure that nearest player is not on same team as this object
                    {
                        // Damage the target by the determined amount
                        player.GetComponent<UnitHealth>().DamageTarget(damage, weaponShooter, teamNumberShooter, damagerTag);

                    }


                }
            }

        }

        // Destroy the bullet
        PhotonNetwork.Destroy(gameObject);
        Destroy(target.gameObject);
    }

}
