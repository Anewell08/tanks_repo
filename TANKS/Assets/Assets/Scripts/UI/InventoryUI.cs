﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    Inventory inventory;                // Used to store reference to inventory
    public PlayerManager _target;       // Used to reference the player the UI is attached to

    public Transform itemsParent;       // Items parent of the inventory slots

    InventorySlot[] slots;              // Array of the inventory slots

    void Awake()
    {
        this.GetComponent<Transform>().SetParent(GameObject.Find("Canvas").GetComponent<Transform>(),false); // Moves this UI to the canvas


    }

    // Stores reference to the player the UI is attached to.
    public void SetTarget(PlayerManager target)
    {
        if (target == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> PlayMakerManager target for PlayerUI.SetTarget.", this);
            return;
        }
        // Cache references for efficiency
        _target = target;
    }

    private void Start()
    {
        inventory = _target.GetComponent<PlayerManager>().Inventory;    // Gets reference to inventory
        inventory.onItemChangedCallback += UpdateUI;                    // When this event is called in inventory, update the UI
        

        slots = itemsParent.GetComponentsInChildren<InventorySlot>();   // Sets slots array to be equal to all the inventory slots
        UpdateUI();                                                     // Calls update UI to make sure UI is correct if player just respawned with items
    }

    private void Update()
    {
        // Destroys the UI if the player dies.
        if (_target == null)
        {
            Destroy(this.gameObject);
            return;
        }
    }

    // Loops through the slots, adding item to slot if the inventory has an item to put there. Else clears the slot. This is what moves the items to the front when an item is dropped
    void UpdateUI()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.items.Count)
            {
                slots[i].AddItem(inventory.items[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }

    }

}
