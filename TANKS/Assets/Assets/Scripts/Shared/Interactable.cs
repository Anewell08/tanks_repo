﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : Photon.MonoBehaviour
{
    public float radius = 1.5f;                     // Radius used to know when to interact
    public Transform interactionTransform;          // Transform of this gameobject, where the interaction takes place

    bool isFocus = false;                           // Set to false on start as nobody is focusing on start
    Transform player;                               // Transform of interacting player
    protected GameObject interactingPlayer;         // Gameobject of interacting player

    bool hasInteracted = false;                     // Bool to know whether interaction has taken place

    // Allowed to be overriden, as we want to change what interact does depending on what the object is
    public virtual void Interact()
    {
        // This probably needs to be moved to another script. This removes the item from the inventory
        if (interactingPlayer.GetComponent<PlayerController>().droppingItem == true)
        {
            RemoveItem();
        }
    }

    public virtual void RemoveItem()
    {        
        Item item = interactingPlayer.GetComponent<PlayerController>().itemToBeDropped;
        interactingPlayer.GetComponent<PhotonView>().RPC("PunRemove", PhotonTargets.AllViaServer, item.dictionaryString);
        // Do I need to add something here to add functionality to allow this object to receive the item?

    }

    public virtual void ChangeActive(bool setValue) { }


    // When this gameobject is focus, set the following parameters
    public void OnFocused(Transform playerTransform, GameObject playerObject)
    {
        isFocus = true;                                                             // Used in update, only allow interaction if this is the item that was focused
        player = playerTransform;                                                   // Set the player trying to interact
        hasInteracted = false;                                                      // Make sure that when the item is focused, you don't immediately interact
        interactingPlayer = playerObject;                                           // Used in interact to find references to the gameobjects components      
    }

    public void OnDefocused()
    {
        isFocus = false;
        player = null;
        hasInteracted = false;
        interactingPlayer = null;
    }

    void Update()
    {
        // If this item has been focused and hasn't been interacted with, and the distance between the player and this gameobject comes inside radius, interact with object
        if (isFocus == true && hasInteracted == false)
        {
            float distance = Vector3.Distance(player.position, interactionTransform.position);
            if (distance <= radius)
            {
                Interact();
                hasInteracted = true;
                player.GetComponent<PlayerController>().hasInteractedWithObject = true;
            }
        }
    }

    // Creates radius in scene view
    private void OnDrawGizmosSelected()
    {
        if (interactionTransform == null)
        {
            interactionTransform = transform;
        }
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }

}
