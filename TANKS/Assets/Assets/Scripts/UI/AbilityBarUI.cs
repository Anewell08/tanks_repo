﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityBarUI : MonoBehaviour {

    public PlayerManager _target;       // Used to reference the player the UI is attached to
    public GameObject[] abilityButtons = new GameObject[6];

    void Awake()
    {
        this.GetComponent<Transform>().SetParent(GameObject.Find("Canvas").GetComponent<Transform>(), false); // Moves this UI to the canvas


    }

    public void SetTarget(PlayerManager target)
    {
        if (target == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> PlayMakerManager target for PlayerUI.SetTarget.", this);
            return;
        }
        // Cache references for efficiency
        _target = target;

        SetAbilities();
    }

    public void SetAbilities()
    {
        for (int i = 0; i < abilityButtons.Length; i++)
        {
            abilityButtons[i].GetComponent<AbilityGrabber>().targetScript = _target.abilityScripts[i];
            abilityButtons[i].GetComponent<AbilityGrabber>().SetImage();
        }


    }

    private void Update()
    {
        // Destroys the UI if the player dies.
        if (_target == null)
        {
            Destroy(this.gameObject);
            return;
        }
    }
}
