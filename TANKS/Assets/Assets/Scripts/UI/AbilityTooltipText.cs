﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityTooltipText : MonoBehaviour
{
    public Text abilityText;
    public Image abilityTextBackground;
    

    private float level;


    void Start()
    {

    }


    void Update()
    {

    }

    public void SetActive()
    {
        if (gameObject.GetComponent<AbilityGrabber>().targetScript.abilityTextString != "")
        {
            abilityTextBackground.gameObject.SetActive(true);
            abilityText.text = gameObject.GetComponent<AbilityGrabber>().targetScript.abilityTextString;
        }        
    }

    public void SetInactive()
    {
        abilityTextBackground.gameObject.SetActive(false);


    }

}
