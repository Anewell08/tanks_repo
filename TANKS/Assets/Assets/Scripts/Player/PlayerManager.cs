﻿#if UNITY_5 && (!UNITY_5_0 && !UNITY_5_1 && !UNITY_5_2 && ! UNITY_5_3) || UNITY_2017
#define UNITY_MIN_5_4
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerManager : Photon.MonoBehaviour, IPunObservable
{
    #region Variables

    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;


    // These are used to update player position on network
    private Vector3 latestCorrectPos;
    private Quaternion latestCorrectRot;
    private Vector3 onUpdatePos;
    private Quaternion onUpdateRot;
    private float fraction;
    
    // UI prefabs set in inspector
    [Tooltip("The Player's UI GameObject Prefab")]
    public GameObject PlayerUiPrefab;
    public GameObject CurrencyUiPrefab;
    public GameObject inventoryUiPrefab;
    public GameObject abilityBarPrefab;
    public GameObject SelectionUI;
    public SelectionInventoryUI selectionUIScript;

    // Reference to the inventory of the player
    public Inventory Inventory;

    // Player's current health and currency. Not needed in inspector, but needed from other scripts
    [HideInInspector]
    public float currency;
    [HideInInspector]
    public int playerLevel = 1;

    // Attributes set in inspector
    [Tooltip("Attributes")]
    public float maxHealth;
    public float movementSpeed;
    public AbilityScript[] abilityScripts = new AbilityScript[6];

    // These are called from the Tank Weapon script to be able to rotate the firepoint and know where to fire from
    [Tooltip("Unity Setup Fields")]
    public Transform partToRotate;
    public Transform partToRotateChildBegRot;
    public Transform firepoint;

    // Used to store reference to Game Manager
    private GameManager gameManager;
    private TeamManager teamManager;

    #endregion

    #region Start and Awake
    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during initialization phase.
    /// </summary>
    void Start()
    {
        // Sets the position and rotation of these variables to both send and receive from other players
        this.latestCorrectPos = transform.position;
        this.onUpdatePos = transform.position;
        this.latestCorrectRot = transform.rotation;
        this.onUpdateRot = transform.rotation;
        

        if (gameManager == null)  //Stores a reference to the Game Manager in order to call to it later to kill and respawn the player.
        {
            gameManager = GameManager.Instance;
        }
        if (Inventory == null)  //Stores a reference to the player's inventory.
        {
            Inventory = GetComponent<Inventory>();
        }
        if (teamManager == null)  //Stores a reference to the Game Manager in order to call to it later to kill and respawn the player.
        {
            teamManager = TeamManager.Instance;
        }
        // Sets level of unit based on current team level
        if (gameObject.GetComponent<TeamNumber>().teamNumber == 1)
        {
            playerLevel = teamManager.currentLevelTeam1;
        }
        else
        {
            playerLevel = teamManager.currentLevelTeam2;
        }

        // Stores reference to camera script on player game object
        Com.NewellGames.TANKS.CameraScript _cameraWork = this.gameObject.GetComponent<Com.NewellGames.TANKS.CameraScript>();

        //Instantiates Player UI, Inventory UI, Currency UI
        if (PlayerUiPrefab != null)
        {
            GameObject _uiGo = Instantiate(PlayerUiPrefab) as GameObject;
            _uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);

        }

        if (PhotonView.isMine)
        {
            if (inventoryUiPrefab != null)
            {
                GameObject _InventoryUI = Instantiate(inventoryUiPrefab) as GameObject;
                _InventoryUI.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
            }
            if (CurrencyUiPrefab != null)
            {
                GameObject _uiGo = Instantiate(CurrencyUiPrefab) as GameObject;
                _uiGo.SendMessage("SetPlayerTargetCurrencyUI", this, SendMessageOptions.RequireReceiver);
            }
            if (abilityBarPrefab != null)
            {
                GameObject _abilityBarUI = Instantiate(abilityBarPrefab) as GameObject;
                _abilityBarUI.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
                for (int i = 0; i < abilityScripts.Length; i++)
                {
                    abilityScripts[i].GetComponent<AbilityScript>().pManager = gameObject.GetComponent<PlayerManager>();
                    abilityScripts[i].GetComponent<AbilityScript>().pController = gameObject.GetComponent<PlayerController>();
                }
            }
            if (SelectionUI != null)
            {
                Debug.Log("selection UI created");
                GameObject _selectionUI = Instantiate(SelectionUI) as GameObject;
                SelectionUI = _selectionUI;
                _selectionUI.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
                selectionUIScript = SelectionUI.GetComponent<SelectionInventoryUI>();
                SelectionUI.SetActive(false);
            }
        }

        //Begins to follow player if the player is the local player
        if (_cameraWork != null)
        {
            if (PhotonView.isMine)
            {
                _cameraWork.OnStartFollowing();
            }
        }
        else
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> CameraWork Component on playerPrefab.", this);
        }
    #if UNITY_MIN_5_4
            // Unity 5.4 has a new scene management. register a method to call CalledOnLevelWasLoaded.
            UnityEngine.SceneManagement.SceneManager.sceneLoaded += (scene, loadingMode) =>
            {
                this.CalledOnLevelWasLoaded(scene.buildIndex);
            };
    #endif

    }

    private void Awake()
    {
        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
        if (PhotonView.isMine)
        {
            PlayerManager.LocalPlayerInstance = this.gameObject;
        }
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(this.gameObject);
    }
    #endregion

    #region Other Functions
    /// <summary>
    /// Sets the health of the player unit to be equal to the float inputted into the function.
    /// </summary>
    /// <param name="currentHealth"></param>

    public void LevelUp(int currentLevel)
    {
        playerLevel = currentLevel;
        gameObject.GetComponent<UnitHealth>().LevelUpHealth(playerLevel);
        for (int i = 0; i < abilityScripts.Length; i++)
        {
            abilityScripts[i].UpdateLevel(playerLevel);
        }
        if (PhotonView.isMine)
        {
            selectionUIScript.UpdateUI();
        }
         

    }

    /// <summary>
    /// Sets the time to respawn for the player, then calls game manager to kill this player and respawn after the set time amount.
    /// </summary>
    public void KillAndRespawn()
    {
        float timeToRespawn = Mathf.Round((gameObject.GetComponent<CurrencyScript>().GetCurrency(gameObject.transform.tag) / 4));         //Time to respawn equals the player's bounty divided by 8
        Debug.Log("Get currency divided by 8 equals " + timeToRespawn + " which is the time to respawn");
        gameManager.CallKillAndRespawn(gameObject, timeToRespawn);                                          //Calls the GameManager script to destroy the unit on the network and respawn them after a set time to respawn
    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //// We own this player: send the others our data
            //stream.SendNext(unitHealth);
            //stream.SendNext(transform.position);
            //stream.SendNext(transform.rotation);

            //Sets the variables to send then sends them to other players
            Vector3 pos = transform.localPosition;      
            Quaternion rot = transform.localRotation;

            stream.Serialize(ref pos);
            stream.Serialize(ref rot);

            
            //int[] itemSendInt = new int[6];
            //for (int i = 0; i < itemSendInt.Length; i++)
            //{
            //    itemSendInt[i] = Inventory.items[i].dictionaryInt;
            //    stream.Serialize(ref itemSendInt[i]);
            //}
                
            



        }
        else
        {
            //// Network player, receive data
            //this.unitHealth = (float)stream.ReceiveNext();
            //this.transform.position = (Vector3)stream.ReceiveNext();
            //this.transform.rotation = (Quaternion)stream.ReceiveNext();

            // Receive latest state information
            Vector3 pos = Vector3.zero;
            Quaternion rot = Quaternion.identity;
            


            stream.Serialize(ref pos);
            stream.Serialize(ref rot);

            this.latestCorrectPos = pos;                // save this to move towards it in FixedUpdate()
            this.onUpdatePos = transform.localPosition; // we interpolate from here to latestCorrectPos
            this.latestCorrectRot = rot;
            onUpdateRot = transform.localRotation;
            this.fraction = 0;                          // reset the fraction we alreay moved. see Update()

            transform.localRotation = rot;              // this sample doesn't smooth rotation

            //int[] itemSendInt = new int[6];
            //for (int i = 0; i < itemSendInt.Length; i++)
            //{
                
            //    stream.Serialize(ref itemSendInt[i]);
            //    Inventory.items[i] = //reference to dictionary to find object that goes with that dictionary int
            //}
            //Inventory.onItemChangedCallback();


        }
    }        

    void Update()
    {
        if (this.PhotonView.isMine)
        {
            return;     // if this object is under our control, we don't need to apply received position-updates 
        }

        // We get 10 updates per sec. Sometimes a few less or one or two more, depending on variation of lag.
        // Due to that we want to reach the correct position in a little over 100ms. We get a new update then.
        // This way, we can usually avoid a stop of our interpolated cube movement.
        //
        // Lerp() gets a fraction value between 0 and 1. This is how far we went from A to B.
        //
        // So in 100 ms, we want to move from our previous position to the latest known. 
        // Our fraction variable should reach 1 in 100ms, so we should multiply deltaTime by 10.
        // We want it to take a bit longer, so we multiply with 9 instead!

        this.fraction = this.fraction + Time.deltaTime * 9.9f;
        transform.localPosition = Vector3.Lerp(this.onUpdatePos, this.latestCorrectPos, this.fraction); // set our pos between A and B
        transform.localRotation = Quaternion.Lerp(this.onUpdateRot, this.latestCorrectRot, this.fraction);    // set our rotation between A and B
    }

    #if !UNITY_MIN_5_4
    /// <summary>See CalledOnLevelWasLoaded. Outdated in Unity 5.4.</summary>

    void OnLevelWasLoaded(int level)
    {
       this.CalledOnLevelWasLoaded(level);
    }
    #endif

    void CalledOnLevelWasLoaded(int level)
    {
        // check if we are outside the Arena and if it's the case, spawn around the center of the arena in a safe zone
        if (!Physics.Raycast(transform.position, -Vector3.up, 5f))
        {
            transform.position = new Vector3(0f, 5f, 0f);
        }

        // When the level is loaded get the health of the player and instantiate the player
        GameObject _uiGo = Instantiate(this.PlayerUiPrefab) as GameObject;
        _uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
    }
    #endregion
} 