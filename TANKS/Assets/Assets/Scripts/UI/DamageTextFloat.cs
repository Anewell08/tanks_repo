﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTextFloat : MonoBehaviour
{
    [HideInInspector]
    public Vector2 screenOffset = new Vector2(0,0);

    [HideInInspector]
    public bool positionSet = false;
    Vector2 additionVector = new Vector2(0, 0.25f);

    private GameObject target;

    //private void Awake()
    //{
    //    this.transform.SetParent(GameObject.Find("Canvas").transform, false);
    //    screenPosition = this.transform.position;
    //}

    public void SetPosition(GameObject GO)
    {
        target = GO;
        //screenPosition = Camera.main.WorldToScreenPoint(GO.transform.position);
        //this.transform.position = screenPosition;
        //screenPosition = this.transform.position;
        //positionSet = true;
    }

    private void Update()
    {
        if (positionSet == true)
        {

            screenOffset = screenOffset + additionVector;
            if (target == null)
            {
                Destroy(gameObject);
                return;
            }
            this.transform.position = (Vector2)Camera.main.WorldToScreenPoint(target.transform.position) + screenOffset;
        }
    }

}
