﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameManager : Photon.PunBehaviour
{
    #region Public Properties


    // Creating the static instance of the game manager
    static public GameManager Instance;

    [Tooltip("The prefab to use for representing the player")]
    public GameObject playerPrefab;

    public bool UnitUIOn = true;

    #endregion

    #region MonoBehavior Methods

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        UnitUIOn = true;
        // Spawns player if there is not currently one on scene start    
        if (playerPrefab == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
        }
        else
        {
            if (PlayerManager.LocalPlayerInstance == null)
            {
                Debug.Log("We are Instantiating LocalPlayer from " + Application.loadedLevelName);
                // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                //PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
            }
            else
            {
                Debug.Log("Ignoring scene load for " + Application.loadedLevelName);
            }
              
        }
    }

    private void Update()
    {

    }

    #endregion

    #region Photon Messages


    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }

    /// <summary>
    ///  Called when there is a player connected to the room
    /// </summary>
    /// <param name="other"></param>
    public override void OnPhotonPlayerConnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerConnected() " + other.NickName); // not seen if you're the player connecting


        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected


            //LoadArena();
        }
    }

    /// <summary>
    /// Called when a player disconnects or otherwise leaves the room
    /// </summary>
    /// <param name="other"></param>
    public override void OnPhotonPlayerDisconnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerDisconnected() " + other.NickName); // seen when other disconnects


        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("OnPhotonPlayerDisonnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected


            //LoadArena();
        }
    }


    #endregion


    #region Public Methods

    /// <summary>
    /// Leave the current room and return to master server.
    /// </summary>
    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
        Debug.Log("I clicked the button to leave the room");
    }


    /// <summary>
    /// Starts the coroutine that destroys the current player and respawns him after a set respawn time.
    /// </summary>
    /// <param name="playerToKill"></param>
    /// <param name="respawnT"></param>
    // Called from the player manager and given a gameObject and a time to take to respawn
    public void CallKillAndRespawn(GameObject playerToKill, float respawnT)
    {
        StartCoroutine(KillAndRespawnPlayer(playerToKill, respawnT));
    }

    public IEnumerator KillAndRespawnPlayer(GameObject player, float respawnTime)
    {
        float returnCurrency = 0;                                                                                                           // resets the currency to return to the player to 0
        List<Item> itemsSaved = new List<Item>();                                                                                           // creates a new blank list to store inventory items in
        itemsSaved = player.GetComponent<PlayerManager>().Inventory.items;                                                                  // puts player's current inventory in the blank list
        returnCurrency = player.GetComponent<PlayerManager>().currency;                                                                     // gets the player's currency balance and stores it
        PhotonNetwork.Destroy(player);                                                                                                      // Destroys (kills) the player
        yield return new WaitForSeconds(respawnTime);                                                                                       // Waits for the respawn time before spawning player again
        GameObject newPlayer = PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);          // instantiates the player at center of map
        newPlayer.GetComponent<PlayerManager>().currency = returnCurrency;                                                                  // sets the player's currency again to what he had before
        newPlayer.GetComponent<Inventory>().items = itemsSaved;                                                                             // Sets the player's inventory to what it was before
        foreach (var item in newPlayer.GetComponent<Inventory>().items)                                                                     // Instantiates each item gameobject to be a child of player
        {
            GameObject itemCreated;
            itemCreated = Instantiate(item.itemPrefab);
            itemCreated.transform.parent = newPlayer.gameObject.transform;
            itemCreated.transform.position = newPlayer.transform.position;
            itemCreated.transform.rotation = newPlayer.transform.rotation;
        }
    }


    #endregion

    #region Private Methods

    /// <summary>
    /// Loads the correct scene based on player count. Will not load if player is not master client.
    /// </summary>
    // Needs to change eventually as we do not want to load a new scene each time a player joins or exits.
    //void LoadArena()
    //{

    //    if (!PhotonNetwork.isMasterClient)
    //    {
    //        Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
    //    }
    //    Debug.Log("PhotonNetwork : Loading Level : " + PhotonNetwork.room.PlayerCount);
    //    if (PhotonNetwork.room.PlayerCount == 2)
    //    {
    //        Debug.Log("returning load arena");
    //        return;
    //    }
    //    else
    //    {
    //        PhotonNetwork.LoadLevel("TankTest");
    //    }
        
    //}


    #endregion
}

