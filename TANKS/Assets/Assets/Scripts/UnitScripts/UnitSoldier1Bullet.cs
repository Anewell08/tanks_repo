﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSoldier1Bullet : Bullet {

    public GameObject muzzleFlash;
    private GameObject MF;

    private void Awake()
    {
        MF = Instantiate(muzzleFlash, transform.position,muzzleFlash.transform.rotation);
        Destroy(MF, 0.1f);
    }

    public override void Seek(Transform _target, float weaponDamage, GameObject shooter, int teamNumber, string tag)
    {
        base.Seek(_target, weaponDamage, shooter, teamNumber, tag);
        MF.transform.parent = shooter.transform;
    }
}
