﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{



    public delegate void OnItemChanged();           // Creation of item changed event
    public OnItemChanged onItemChangedCallback;


    public int space = 6;                           // Inventory space

    public List<Item> items = new List<Item>();     // List of items
    public ItemDictionary itemDictionaryScript;

    private void Awake()
    {
        itemDictionaryScript = GetComponent<ItemDictionary>();
    }

    /// <summary>
    /// Adds the item passed into function into the inventory if there is space. Creates item prefab as child to gameobject.
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    [PunRPC]
    public bool PunAdd(string itemString,int photonID) // put in 0 for photonID if the object does not need to be destroyed
    {
        Item item = itemDictionaryScript.itemDictionary[itemString];
        // If there is space left in inventory, add item to the list, and instantiate the item prefab, and call the item event
        if (!item.isDefaultItem)
        {
            
            if (items.Count >= space)
            {
                Debug.Log("Not any space left");
                return false;                       // return false if the item cannot be added
            }
            items.Add(item);
            GameObject itemCreated;
            itemCreated = PhotonNetwork.Instantiate(item.itemPrefab.name,transform.position,transform.rotation,0);
            itemCreated.transform.parent = gameObject.transform;                                // Set the parent of the created item prefab to this gameobject
            itemCreated.transform.position = itemCreated.transform.parent.transform.position;
            itemCreated.transform.rotation = itemCreated.transform.parent.transform.rotation;
            if (item.isTankWeapon == true)
            {
                itemCreated.GetComponent<TankWeapon>().UpdateTeamNumber();
            }      
            if (onItemChangedCallback != null)                                                  // Calls item changed
            {
                onItemChangedCallback.Invoke();
            }
            
        }
        if (photonID != 0)
        {
            PhotonNetwork.Destroy(PhotonView.Find(photonID).gameObject);
        }

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            player.GetComponent<PlayerManager>().selectionUIScript.UpdateUI();
        }

        return true; // return true if the item is added
    }

    /// <summary>
    /// Removes the item from inventory and destroys the item prefab associated with it that is attached to the gameobject.
    /// </summary>
    /// <param name="item"></param>
    [PunRPC]
    public void PunRemove(string itemString)
    {
        Item item = itemDictionaryScript.itemDictionary[itemString];
        string name = (item.itemPrefab.name) + "(Clone)";       // Just finding the item prefab name doesn't work because it is a clone.
        items.Remove(item);                                     // Removes the item from the inventory list
        PhotonNetwork.Destroy(transform.Find(name).gameObject);               // Destroys the item prefab associated with it
        if (onItemChangedCallback != null)                      // Calls item changed
        {
            onItemChangedCallback.Invoke();
        }
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            player.GetComponent<PlayerManager>().selectionUIScript.UpdateUI();
        }
    }

}
