﻿using System;
using UnityEngine;

public class Bullet : Photon.MonoBehaviour {

    protected Transform target;                       // Target of the bullet
    protected float damage;                           // Damage of the bullet as determined by the weapon
    protected GameObject weaponShooter;               // Object that shot the bullet
    protected int teamNumberShooter;
    protected string damagerTag;
    private Vector3 startPos;
    public float speed = 10f;                       // Speed of the bullet, usually overridden in the inspector
    public float explosionRange;

    

    private float currentDuration;
    private float duration;

    private Vector3 arcVector = new Vector3(0,1.5f,0);
    public float arcMultiplier = 5;


    public GameObject ImpactEffect;                 // Impact object of the bullet
    /// <summary>
    /// Sets the target, damage, shooter of the bullet, team number of shooter, and tag of shooter.
    /// </summary>
    /// <param name="_target"></param>
    /// <param name="weaponDamage"></param>
    /// <param name="shooter"></param>
    public virtual void Seek(Transform _target, float weaponDamage, GameObject shooter, int teamNumber, string tag)
    {
        target = _target;
        damage = weaponDamage;
        weaponShooter = shooter;
        teamNumberShooter = teamNumber;
        damagerTag = tag;
        duration = Vector3.Distance(target.transform.position, this.transform.position) / speed;
        arcVector = arcVector * arcMultiplier;
        startPos = this.transform.position;

    }
	
	
	void Update ()
    {
        // If there is not target, destroy the bullet and don't do anything else.
        if (target == null)
        {
            if (PhotonView.isMine)
            {
                PhotonNetwork.Destroy(gameObject);
            }


            return;
        }

        #region oldUpdate
        //// Find the direction of the target
        //Vector3 dir = target.position - transform.position;

        //// Determine how far to go this frame
        //float distanceThisFrame = speed * Time.deltaTime;

        //// If the distance has reached the target, call hit target and don't do anything else.
        //if (dir.magnitude <= distanceThisFrame)
        //{
        //    HitTarget();
        //    return;
        //}

        //// Move in the direction of the target by the distance determined.
        //transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        #endregion

        this.transform.position = CalculateBezierPoint(currentDuration / duration, startPos, target.transform.position, (startPos + ((target.transform.position - startPos) * 0.5f)) + arcVector);
        transform.rotation = Quaternion.LookRotation(target.position - transform.position);    // Looks in that direction
        currentDuration += Time.deltaTime;
        if (PhotonView.isMine)
        {
            if (currentDuration >= duration)
            {
                HitTarget();
            }
        }




    }

    private Vector3 CalculateBezierPoint(float t, Vector3 startPosition, Vector3 endPosition, Vector3 controlPoint)
    {
        float u = 1 - t;
        float uu = u * u;

        Vector3 point = uu * startPosition;
        point += 2 * u * t * controlPoint;
        point += t * t * endPosition;

        return point;
    }

    /// <summary>
    /// Called when bullet reaches the target.
    /// </summary>
    public virtual void HitTarget()
    {
        // If there is not target, destroy the bullet and don't do anything else.
        if (target == null)
        {
            if (PhotonView.isMine)
            {
                PhotonNetwork.Destroy(gameObject);
            }            

            return;
        }

        // Create the impact effect, then destroy it after two seconds.
        //GameObject effectIns = PhotonNetwork.Instantiate(ImpactEffect.name, transform.position, transform.rotation,0);
        //Destroy(effectIns, 2);

        // Damage the target by the determined amount
        //target.GetComponent<UnitHealth>().DamageTarget(damage, weaponShooter, teamNumberShooter, damagerTag);

        // Destroy the bullet
        if (PhotonView.isMine)
        {
            PhotonNetwork.Destroy(gameObject);
        }
        
    }
}
