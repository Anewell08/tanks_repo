﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitHealth : MonoBehaviour
{
    // These are set inside the script but need to be accessed by other scripts
    public float currentHealth;         //Current health of unit
    public float maxHealth = 500;       //Max health of unit, set by player manager or unit manager

    private PlayerManager playerManager;    //Stored reference to player manager
    private UnitManager unitManager;        //Stored reference to unit manager
    private TeamManager teamManager;

    public Text damageText;

    private void Start()
    {
        //If tag is player, access and store player manager and get the max health, set the current health to max health, and then update the player manager to know we're at max health.
        if (this.gameObject.tag == "Player")
        {
            playerManager = this.gameObject.GetComponent<PlayerManager>();
            maxHealth = playerManager.maxHealth;
            currentHealth = maxHealth;
            StartHealth(playerManager.playerLevel);
        }
        // If tag isn't player, access and store reference to unit manager then do all the same as above.
        if (this.gameObject.tag != "Player")
        {
            unitManager = this.gameObject.GetComponent<UnitManager>();
            maxHealth = unitManager.maxHealth;
            currentHealth = maxHealth;
            StartHealth(unitManager.unitLevel);
        }
        if (teamManager == null)  //Stores a reference to the Game Manager in order to call to it later to kill and respawn the player.
        {
            teamManager = TeamManager.Instance;
        }
    }

    private void CreateFloatingText(float damage)
    {
        Vector2 screenPositionUH = Camera.main.WorldToScreenPoint(transform.position);
        Text floatingText = Instantiate(damageText);
        floatingText.transform.SetParent(GameObject.Find("Canvas").transform, false);
        floatingText.transform.position = screenPositionUH;
        floatingText.GetComponent<DamageTextFloat>().SetPosition(gameObject);
        //floatingText.GetComponent<DamageTextFloat>().screenOffset = screenPositionUH;
        floatingText.GetComponent<DamageTextFloat>().positionSet = true;
        floatingText.text = Mathf.RoundToInt(damage).ToString();
        Destroy(floatingText.gameObject, 1);


    }


    /// <summary>
    /// Damage the target by a certain amount.
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="damager"></param>
    //Called from bullet script. Takes a damage amount and the unit who hurt this unit
    public void DamageTarget(float damage, GameObject damager, int teamNumberOfShooter,string damagerTag)
    {
        if (damage > (maxHealth * 0.01f))
        {
            CreateFloatingText(damage);
        }


        // If you reduce the unit's health below or equal to 0, give bounty through currency script then kill the unit through death function
        if ((currentHealth - damage) <= 0)
        {
            currentHealth = 0;
            // Right now it seems like currency or experience is only added if damager isn't already dead by the time this runs. Might need to fix
            if (damager != null)
            {
                if (damager.gameObject.tag == "Player")
                {
                    damager.GetComponent<PlayerManager>().currency += this.gameObject.GetComponent<CurrencyScript>().GetCurrency(damagerTag);
                }
                teamManager.UpdateExperience(teamNumberOfShooter, gameObject.GetComponent<ExperienceScript>().GetExperience(damagerTag));
            }
            Death();

        }
        else // If the unit's health doesn't go below 0, just reduce the health by the damage amount.
        {
            currentHealth -= damage;
        }

        // Update the unit's health based on whether it is a player or regular unit
    }   

    /// <summary>
    /// Heal the unit at the control point for a percentage of its max health.
    /// </summary>
    // Called from the control point script every 0.33 seconds to heal the unit.
    public void ControlPointHeal()
    {
        // Only heals player units, so returns if the player manager returns null
        if (playerManager == null)
        {
            return;
        }
        // Don't heal and return if the unit is at max health already.
        if (currentHealth == maxHealth)
        {
            return;
        }
        // We want to heal for a minimum of 6% (0.02 * 3 times per second) of the player's max health per second, so if the normal healing is less than that, heal for 6% of max health per second.
        if ((maxHealth - currentHealth) * 0.1f < maxHealth * 0.02f)
        {
            if ((currentHealth += maxHealth * 0.02f) > maxHealth) // 6% per second would result in player going over max health, so set current health to max health
            {

                currentHealth = maxHealth;
            }
            else
            {
                // Player is healing less than 6% of max health per second, so health increases by min of 6% per second"
                currentHealth += maxHealth * 0.02f;
            }
            
        }
        else
        {
            currentHealth += (maxHealth - currentHealth) * 0.1f; // Player heals for 30% of missing health per second as this runs every 0.33 seconds.
        }
        
    }

    /// <summary>
    /// Destroys the unit. If the unit is a player, it will respawn after a set time.
    /// </summary>
    public void Death()
    {
        if (gameObject.tag == "Player")
        {
            playerManager.KillAndRespawn(); // Calls the Kill and Respawn script in the player manager which calls the correct game manager script to kill and respawn player
        }
        else
        {
            if (PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.Destroy(gameObject);// If unit is not a player, simply destroy it    
            }
        }
    }

    void StartHealth(int level)
    {
        for (int i = 1; i <= level; i++)
        {
            maxHealth = Mathf.RoundToInt((maxHealth * (1 + ((float)i / 60))));
            currentHealth = Mathf.RoundToInt((currentHealth * (1 + ((float)i / 60))));
        }


    }

    public void LevelUpHealth(int level)
    {
        maxHealth = Mathf.RoundToInt((maxHealth * (1 + ((float)level / 60))));
        currentHealth = Mathf.RoundToInt((currentHealth * (1 + ((float)level / 60))));        
    }
}
