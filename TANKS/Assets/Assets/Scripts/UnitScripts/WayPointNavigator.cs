﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class WayPointNavigator : Photon.MonoBehaviour
{

    public Vector3[] points;
    public int destPoint = 1;
    public NavMeshAgent agent;
    public float remainingDistance;
    public Vector3[] pointArray;


    private bool pointsSetUp;

    void Start()
    {

        if (agent == null)
        {
            agent = GetComponent<NavMeshAgent>();
        }

        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        agent.autoBraking = false;

        agent.updatePosition = false;

        if (PhotonNetwork.isMasterClient)
        {
            for (int i = 0; i < points.Length; i++)
            {
                pointArray[i] = points[i];
            }
            PhotonView.RPC("PunGivePoints", PhotonTargets.OthersBuffered, pointArray);
            FindClosestPoint();
        }
    }

    [PunRPC]
    public void PunGivePoints(Vector3[] pointLocations)
    {
        for (int i = 0; i < pointLocations.Length; i++)
        {
            points[i] = pointLocations[i];
        }
        FindClosestPoint();


    }

    void GotoNextPoint()
    {
        if (points[0] == null)
        {
            pointsSetUp = false;
            return;
        }
        // Returns if no points have been set up
        if (points.Length == 0)
            return;
        //The previous point has been reached, so lets go to the next one.
        destPoint = (destPoint + 1);
        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint] + new Vector3(Random.Range(0, 2), 0, Random.Range(0, 2));




    }

    public void FindClosestPoint()
    {
        if (agent == null)
        {
            agent = GetComponent<NavMeshAgent>();
        }
        if (points[0] == null)
        {
            pointsSetUp = false;
            return;
        }
        //If the closest point is the current point, go to that one, but if the closest point is the one after that, go to that one
        if (Vector3.Distance(transform.position, points[destPoint]) > Vector3.Distance(transform.position, points[destPoint + 1]))
        {
            destPoint = (destPoint + 1);
            agent.destination = points[destPoint];
        }
        else
        {
            agent.destination = points[destPoint];
        }

        agent.SetDestination(points[destPoint]);



    }

    


    void Update()
    {
        // Choose the next destination point when the agent gets
        // close to the current one.
        if (!agent.pathPending && Vector3.Distance(gameObject.transform.position, points[destPoint]) < remainingDistance)
        {
            GotoNextPoint();
        }
    }
}
