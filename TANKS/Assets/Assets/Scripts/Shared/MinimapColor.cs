﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapColor : MonoBehaviour {

    public Material team1color;
    public Material team2color;
	void Start ()
    {

	}

    public void SetColor()
    {
        if (gameObject.transform.parent.GetComponent<TeamNumber>().teamNumber == 1)
        {
            gameObject.GetComponent<MeshRenderer>().material = team1color;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().material = team2color;
        }

    }
}
