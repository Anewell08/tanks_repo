﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponTooltipText : MonoBehaviour {

    public Text weaponDamageText;
    public Text weaponRangeText;
    public Text weaponFireRateText;
    public GameObject inventorySlot;

    private float damage;
    private float firerate;
    private float range;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetActive()
    {
        if (inventorySlot.GetComponent<InventorySlot>().item != null)
        {
            gameObject.SetActive(true);
            damage = inventorySlot.GetComponent<InventorySlot>().item.damage;
            firerate = inventorySlot.GetComponent<InventorySlot>().item.fireRate;
            range = inventorySlot.GetComponent<InventorySlot>().item.range;
            weaponDamageText.text = "Damage: " + damage;
            weaponFireRateText.text = "Firerate: " + (1 / firerate);
            weaponRangeText.text = "Range: " + range;
        }
        

    }

    public void SetInactive()
    {
        gameObject.SetActive(false);


    }

}
