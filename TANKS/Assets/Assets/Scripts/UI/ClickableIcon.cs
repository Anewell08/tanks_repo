﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickableIcon : MonoBehaviour {

    Inventory inventory;    // Reference to inventory of target. Not currently used

    private bool rightClickToSelect = false; // Bool used to know when to move icon along with mouse
    public Image icon;                       // Icon of item

    public void Start()
    {
        inventory = GetComponentInParent<InventoryUI>()._target.Inventory; // Stores reference to inventory
    }

    public void OnRightClick()
    {
        // Sets right click to true to allow icon to move in update. Else resets icon to original position if you are already moving it
        if (rightClickToSelect == false)
        {
            rightClickToSelect = true;
        }
        else if (rightClickToSelect == true)
        {
            rightClickToSelect = false;
            icon.rectTransform.offsetMin = new Vector2(2, 2);
            icon.rectTransform.offsetMax = new Vector2(-2, -2);
        }

    }

    public void OnLeftClick()
    {
        // Nothing happens unless you are already moving item. If you are, either Uses or places item
        if (rightClickToSelect == true)
        {
            GetComponentInParent<InventoryUI>()._target.GetComponent<PlayerController>().UseOrPlaceItem(gameObject.GetComponentInParent<InventorySlot>().item); // Uses or places the item that is in the inventory slot parent of this button
            rightClickToSelect = false;
            icon.rectTransform.offsetMin = new Vector2(2, 2);
            icon.rectTransform.offsetMax = new Vector2(-2, -2);
        }


    }

    private void Update()
    {
        // If you used right click to move item, move icon along with mouse
        if (rightClickToSelect == true)
        {
            icon.transform.position = Input.mousePosition;
        }

        if (rightClickToSelect == true)
        {
            // If you left click after moving icon around after right clicking, invoke on Left Click and use or move item
            if (Input.GetMouseButtonDown(0) == true)
            {

                OnLeftClick();


            }
            // If you click any button except left click after right clicking, reset item to original spot
            else if (Input.anyKeyDown == true)
            {
                rightClickToSelect = false;
                icon.rectTransform.offsetMin = new Vector2(2, 2);
                icon.rectTransform.offsetMax = new Vector2(-2, -2);
            }
        }
    }
}
