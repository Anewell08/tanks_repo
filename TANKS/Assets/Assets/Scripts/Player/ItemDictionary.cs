﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDictionary : MonoBehaviour
{
    //First entry is key, second is value. Used to send strings over network since items cannot be sent over network
    public Dictionary<string, Item> itemDictionary = new Dictionary<string, Item>();
    public Item tankWeapon;
    public Item fireballCannon;
    public Item iceSpear;
    void Start()
    {
        itemDictionary.Add("TankWeapon", tankWeapon);
        itemDictionary.Add("FireballCannon", fireballCannon);
        itemDictionary.Add("IceSpear", iceSpear);
    }
}
