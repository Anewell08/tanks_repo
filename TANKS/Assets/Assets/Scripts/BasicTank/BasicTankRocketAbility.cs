﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTankRocketAbility : AbilityScript
{
    public float range;
    public GameObject targetCircle;
    public LayerMask movementMask;              // Masked used to know whether we can click on ground area to move or click
    public GameObject bulletPrefab;
    public GameObject smokeLaunchEffect;


    private GameObject targetCircle2;


    private bool RocketPreparedToFire;
    private bool inRange = false;
    private bool locationSelected;
    private Vector3 fireTargetLocation;
    private bool unitSelected;
    Interactable interactable;
    private bool moveUnit;
    private Vector3 terrainOffset = new Vector3(0,1,0);

    public override void Start()
    {
        base.Start();
        abilityTextString = "Launch a missle that deals " + Mathf.Round(100f * levelBoostPercentage) + " damage to units in the area.";
    }

    public override void OnLeft()
    {
        
        if (countdown <= 0 && locationSelected == false)
        {
            Debug.Log("Rocket prepared to fire");
            targetCircle2 = Instantiate(targetCircle, Camera.main.ScreenToWorldPoint(Input.mousePosition),targetCircle.transform.rotation); // not drawing a circle
            targetCircle2.GetComponent<Projector>().orthographicSize = bulletPrefab.GetComponent<Bullet>().explosionRange;
            RocketPreparedToFire = true;
            inRange = false;
        }
        
    }

    public override void UpdateLevel(int unitPlayerLevel)
    {
        base.UpdateLevel(unitPlayerLevel);
        abilityTextString = "Launch a missle that deals " + Mathf.Round(100f * levelBoostPercentage) + " damage to units in the area.";
    }

    [PunRPC]
    public void PunLaunchRocket()
    {
        StartCoroutine("Launch");

    }

    public IEnumerator Launch()
    {
        //launch bullet
        GameObject bulletGO = PhotonNetwork.Instantiate("BasicTankRocketBulletPrefab", pManager.firepoint.position, pManager.firepoint.rotation,0);
        Bullet bullet = bulletGO.GetComponent<Bullet>();
        Transform targetTransform = new GameObject().transform;
        targetTransform.position = fireTargetLocation;
        if (bullet != null)
        {
            bullet.Seek(targetTransform, Mathf.Round(100f * levelBoostPercentage), this.gameObject, gameObject.GetComponent<TeamNumber>().teamNumber, this.gameObject.transform.tag);
        }
        GameObject effectIns = PhotonNetwork.Instantiate("BasicTankRocketSmokeEffect", pManager.firepoint.position, pManager.firepoint.rotation,0);
        effectIns.transform.rotation *= Quaternion.Euler(-90,0,0);
        effectIns.transform.parent = pManager.firepoint.transform;
        
        

        countdown = 1f / fireRate;
        yield return new WaitForSeconds(5);
        PhotonNetwork.Destroy(effectIns);

    }

    public override void Update()
    {
        base.Update();
        if (RocketPreparedToFire == true)
        {
            if (targetCircle2 != null)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100) && locationSelected == false)
                {
                    targetCircle2.transform.position = hit.point + terrainOffset;

                }
                    
            }
            if (Input.GetMouseButtonDown(0))
            {
                interactable = null;
                unitSelected = false;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;


                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    RocketPreparedToFire = false;
                    return;
                    
                }
                else
                {
                    
                    // if the mouse cast a ray
                    if (Physics.Raycast(ray, out hit, 100))
                    {
                        
                        //check if we hit an interactable collider
                        interactable = hit.collider.GetComponent<Interactable>();
                        // if we did, and it has a unit health, set that as selection
                        if (interactable != null)
                        {
                            Debug.Log("target set on unit");
                            unitSelected = true;
                            if (Vector3.Distance(interactable.transform.position, gameObject.transform.position) < range)
                            {
                                locationSelected = true;
                                inRange = true;
                                fireTargetLocation = interactable.transform.position;

                            }
                            else if (Vector3.Distance(interactable.transform.position, gameObject.transform.position) > range)
                            {
                                locationSelected = true;
                                inRange = false;
                                fireTargetLocation = interactable.transform.position;
                            }
                        }
                        else if (interactable == null)
                        {
                            Debug.Log("target set on movement mask");
                            if (Vector3.Distance(hit.point, gameObject.transform.position) < range)
                            {
                                locationSelected = true;
                                inRange = true;
                                fireTargetLocation = hit.point;

                            }
                            else if (Vector3.Distance(hit.point, gameObject.transform.position) > range)
                            {
                                locationSelected = true;
                                inRange = false;
                                fireTargetLocation = hit.point;
                            }
                        }

                    }
                }
            }
            else if (Input.anyKeyDown)
            {
                RocketPreparedToFire = false;
            }
            if (locationSelected == true)
            {
                if (unitSelected == true)
                {
                    fireTargetLocation = interactable.transform.position;
                    if (Vector3.Distance(fireTargetLocation, gameObject.transform.position) < range)
                    {
                        inRange = true;
                    }
                    else if (Vector3.Distance(interactable.transform.position, gameObject.transform.position) > range)
                    {
                        inRange = false;
                    }
                }
                if (inRange == true)
                {
                    Debug.Log("in range and location selected");
                    Vector3 targetLookAt = new Vector3(fireTargetLocation.x, gameObject.transform.position.y, fireTargetLocation.z); // Finds the vector 3 of the target
                    Vector3 pos = targetLookAt - gameObject.transform.position; // Finds the direction of the target
                    var newRot = Quaternion.LookRotation(pos); // set the quaternion rotation of the direction of the target
                    gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, newRot, Time.deltaTime * 10); // lerp rotate towards the look target  
                    if (Vector3.Dot(transform.forward, (fireTargetLocation - transform.position).normalized) > 0.9f)
                    {
                        playerPhotonView.RPC("PunLaunchRocket", PhotonTargets.AllBufferedViaServer);
                        RocketPreparedToFire = false;
                        locationSelected = false;
                        unitSelected = false;
                        inRange = false;
                    }
                    
                }
                else if (inRange == false && locationSelected == true)
                {
                    Vector3 targetLookAt = new Vector3(fireTargetLocation.x, gameObject.transform.position.y, fireTargetLocation.z); // Finds the vector 3 of the target
                    Vector3 pos = targetLookAt - gameObject.transform.position; // Finds the direction of the target
                    var newRot = Quaternion.LookRotation(pos); // set the quaternion rotation of the direction of the target
                    gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, newRot, Time.deltaTime * 15); // lerp rotate towards the look target    
                    moveUnit = true;
                    if (Vector3.Distance(fireTargetLocation, gameObject.transform.position) < range)
                    {
                        inRange = true;
                        moveUnit = false;
                    }
                }
            }
            
            
        }
        else if (RocketPreparedToFire == false)
        {
            locationSelected = false;
            unitSelected = false;
            inRange = false;
            moveUnit = false;
            if (targetCircle2 != null)
            {
                Destroy(targetCircle2);
            }
        }
    }

    private void FixedUpdate()
    {
        if (moveUnit == true)
        {
            transform.Translate(transform.forward * pManager.movementSpeed * Time.deltaTime, Space.World);
        }
    }
}
