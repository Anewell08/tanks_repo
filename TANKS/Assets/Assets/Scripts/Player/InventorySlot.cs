﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour {

    public Item item;
    public Image icon;

    // Called from inventory UI
    // Sets the item of the inventory item to the item passed through, sets icon to item icon
    public void AddItem(Item newItem)
    {
        item = newItem;

        icon.sprite = item.icon;
        icon.enabled = true;

    }

    // Called from inventory UI
    // Clears the slot, makes item null and resets icon
    public void ClearSlot()
    {
        item = null;

        icon.sprite = null;
        icon.enabled = false;

    }

}
