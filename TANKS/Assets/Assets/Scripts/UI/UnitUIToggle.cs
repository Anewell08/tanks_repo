﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitUIToggle : MonoBehaviour
{
    private bool UnitUIOn = true;
    private GameManager gameManager;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    public void ChangeActive()
    {
        if (UnitUIOn == true)
        {
            UnitUIOn = false;
            gameManager.UnitUIOn = false;
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");                          // Creates empty array of units tagged with "Enemy"
            foreach (GameObject enemy in enemies)
            {
                UnitManager enemyManager = enemy.GetComponent<UnitManager>();
                enemyManager.UnitUiPrefab.SetActive(false);
                if (enemyManager.UnitUICreated == false)
                {
                    enemyManager.SpawnUI();
                }
            }
            enemies = null;
        }
        else
        {
            UnitUIOn = true;
            gameManager.UnitUIOn = true;
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");                          // Creates empty array of units tagged with "Enemy"
            foreach (GameObject enemy in enemies)
            {
                enemy.GetComponent<UnitManager>().UnitUiPrefab.SetActive(true);
            }
            enemies = null;
        }


    }

}
