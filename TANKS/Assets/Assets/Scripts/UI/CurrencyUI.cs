﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrencyUI : MonoBehaviour
{


    #region Public Properties


    [Tooltip("UI Text to display Player's Name")]
    public Text currentCurrencyText;

    [Tooltip("Pixel offset from the player target")]
    public Vector3 ScreenOffset = new Vector3(0f, -60f, 0f);


    #endregion


    #region Private Properties
    PlayerManager _target;

    #endregion


    #region MonoBehaviour Messages

    void Awake()
    {
        this.GetComponent<Transform>().SetParent(GameObject.Find("Canvas").GetComponent<Transform>());

        // Set Max Health

    }

    void Update()
    {
        // Destroy itself if the target is null, It's a fail safe when Photon is destroying Instances of a Player over the network
        if (_target == null)
        {
            Debug.Log("Destroying currency UI");
            Destroy(this.gameObject);
            return;
        }
        currentCurrencyText.text = _target.currency.ToString();
    }

    #endregion


    #region Public Methods




    public void SetPlayerTargetCurrencyUI(PlayerManager target)
    {
        if (target == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> PlayMakerManager target for PlayerUI.SetTarget.", this);
            return;
        }
        // Cache references for efficiency
        _target = target;
        if (currentCurrencyText != null)
        {
            currentCurrencyText.text = "0";
        }
    }

    #endregion
}
