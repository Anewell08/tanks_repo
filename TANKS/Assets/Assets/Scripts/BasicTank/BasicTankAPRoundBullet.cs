﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTankAPRoundBullet : Bullet
{


    /// <summary>
    /// Called when bullet reaches the target.
    /// </summary>
    public override void HitTarget()
    {
        // If there is not target, destroy the bullet and don't do anything else.
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        // Create the impact effect, then destroy it after two seconds.
        GameObject effectIns = (GameObject)Instantiate(ImpactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 5);


        // Damage the target by the determined amount
        //ignorearmormultiplier = 1 / target.getcomponent<armor>().armordamagereduction (if 1 armor reduces damage by 5%, this would be 1/.95)
        //damage = damage * ignorearmormultiplier
        target.GetComponent<UnitHealth>().DamageTarget(damage, weaponShooter, teamNumberShooter, damagerTag);


        // Destroy the bullet
        PhotonNetwork.Destroy(gameObject);
    }


}
