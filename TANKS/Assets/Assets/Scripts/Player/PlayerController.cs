﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : Photon.MonoBehaviour
{
    public int m_PlayerNumber = 1;              // Used to identify which tank belongs to which player.  This is set by this tank's manager.
    [HideInInspector]
    public float movementSpeed = 12f;                 // How fast the tank moves forward and back.

    public float m_TurnSpeed = 180f;            // How fast the tank turns in degrees per second.
    public AudioSource m_MovementAudio;         // Reference to the audio source used to play engine sounds. NB: different to the shooting audio source.
    public AudioClip m_EngineIdling;            // Audio to play when the tank isn't moving.
    public AudioClip m_EngineDriving;           // Audio to play when the tank is moving.
    public float m_PitchRange = 0.2f;           // The amount by which the pitch of the engine noises can vary.
    public LayerMask movementMask;              // Masked used to know whether we can click on ground area to move or click
    private Transform target;                   // Target transform to follow or look at if a target is set
    private float startingAcceleration;         // Used to slow down player as they approach target
    public bool droppingItem;                   // Used to know if player is dropping item on ground or not. Referenced from interactable
    public Item itemToBeDropped;                // The item that needs to be dropped, referenced from clickable button


    //all left wheels
    public GameObject[] LeftWheels;             // Left wheels of the player object
    //all right wheels
    public GameObject[] RightWheels;            // Right wheels of the player object that are rotated

    public GameObject LeftTrack;

    public GameObject RightTrack;

    private GameObject selectionUI;
    private SelectionInventoryUI selectionUIScript;
    private GameObject ShopUI;

    public float wheelsSpeed = 0.5f;            // Speeds of the objects on player
    public float tracksSpeed = 0.5f;
    public float forwardSpeed = 1f;
    public float rotateSpeed = 1f;


    private float moveVertical;                 // The float for moving vertically.
    private float moveHorizontal;               // The float for moving horizontally.
    private Vector3 movement;                   // The movement vector.
    private Rigidbody m_Rigidbody;              // Reference used to move the tank.
    //private float m_MovementInputValue;         // The current value of the movement input.
    //private float m_TurnInputValue;             // The current value of the turn input.
   // private float m_OriginalPitch;              // The pitch of the audio source at the start of the scene.
    private Quaternion lastrotation;                 // The last known rotation of the tank.

    private Camera cam;                         // Used for rays
    private NavMeshAgent agent;                 // Used to move to a location or person
    private Interactable focus;                 // the interactable object that is being followed

    public bool hasInteractedWithObject = false;
    private GameObject clickPoint;

    private void Awake()
    {
        // Set up references
        m_Rigidbody = GetComponent<Rigidbody>();
        cam = Camera.main;
        agent = GetComponent<NavMeshAgent>();
        movementSpeed = GetComponent<PlayerManager>().movementSpeed;
        agent.speed = movementSpeed;
        startingAcceleration = agent.acceleration;
        

    }

    private void Start()
    {
        

    }


    private void OnEnable()
    {
        // When the tank is turned on, make sure it's not kinematic.
        //m_Rigidbody.isKinematic = false;

        // Also reset the input values.
        //m_MovementInputValue = 0f;
        //m_TurnInputValue = 0f;
    }


    private void OnDisable()
    {
        // When the tank is turned off, set it to kinematic so it stops moving.
       // m_Rigidbody.isKinematic = true;
    }


    //private void Start()
    //{
    //    // The axes names are based on player number.
    //    m_MovementAxisName = "Vertical" + m_PlayerNumber;
    //    m_TurnAxisName = "Horizontal" + m_PlayerNumber;

    //    // Store the original pitch of the audio source.
    //    m_OriginalPitch = m_MovementAudio.pitch;
    //}

    
    private void FixedUpdate()
    {
        // Only control the object if the photon view is mine
        if (PhotonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        // Translate the object based on input axis
        transform.Translate(movement.normalized * movementSpeed * Time.deltaTime, Space.World);
    }


    private void Update()
    {
        // Only control the object if the photon view is mine
        if (PhotonView.isMine == false && PhotonNetwork.connected == true)
        {
            return;
        }
        // Make sure there is a camera
        if (cam == null)
        {
            cam = Camera.main;
        }
        // Store the value of both input axes.
        moveVertical = Input.GetAxis("Vertical");
        moveHorizontal = Input.GetAxis("Horizontal");

        
        movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        // If we try to move with the keys, make sure there is no longer a focus and the nav mesh agent stops moving
        if (movement != Vector3.zero)
        {
            if (target != null)
            {
                agent.isStopped = true;
                RemoveFocus();
            }
            // Also reset rigidbody velocity and agent velocity, make sure we are no longer dropping item and that item to be dropped is reset
            //m_Rigidbody.velocity = Vector3.zero;
            agent.velocity = Vector3.zero;
            //m_Rigidbody.angularVelocity = Vector3.zero;
            droppingItem = false;
            itemToBeDropped = null;



        }
        // We only want to move or rotate wheels if we move a certain amount
        if (moveVertical > 0.1f || moveVertical < -0.1f || moveHorizontal >0.1f || moveHorizontal <-0.1f)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movement.normalized), 0.15F);
            //Left wheels rotate
            foreach (GameObject wheelL in LeftWheels)
            {
                wheelL.transform.Rotate(new Vector3(wheelsSpeed, 0f, 0f));
            }
            //Right wheels rotate
            foreach (GameObject wheelR in RightWheels)
            {
                wheelR.transform.Rotate(new Vector3(-wheelsSpeed, 0f, 0f));
            }
            //left track texture offset
            LeftTrack.transform.GetComponent<Renderer>().material.mainTextureOffset += new Vector2(0f, Time.deltaTime * tracksSpeed);
            //right track texture offset
            RightTrack.transform.GetComponent<Renderer>().material.mainTextureOffset += new Vector2(0f, Time.deltaTime * tracksSpeed);
        }
        


        //If we are not moving this frame. 
        //
        // This might be where we add abilities. Or this is where might add an && to the if statement to make sure we aren't using abilities, then add abilities in seperate script
        //
        if (movement == Vector3.zero)
        {
            // and we use right mouse button, set the current focus to the interactable object we clicked on. 
            // This enables follow target and sets the target, which we move to in update function
            if (Input.GetMouseButtonDown(1))
            {
                
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                //if (Physics.Raycast(ray, out hit, 100, movementMask))
                //{
                //    Debug.Log("movement mask");
                //    agent.stoppingDistance = 3;
                //    agent.isStopped = false;
                //    agent.SetDestination(hit.point);                    
                //    if (focus != null)
                //    {
                //        focus.OnDefocused();        // Let the object know it has been defocused
                //    }
                //    // Remove the focus and make sure you are not following anything
                //    focus = null;
                //    agent.speed = movementSpeed;
                //    clickPoint;
                //    clickPoint.transform.position = hit.point;
                //    target = clickPoint.transform;
                    

                //}
                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    return;
                }
                else
                {
                    if (Physics.Raycast(ray, out hit, 100))
                    {
                        Interactable interactable = hit.collider.GetComponent<Interactable>();
                        if (interactable != null)
                        {
                            SetFocus(interactable);

                        }


                    }
                }




            }
            
            


        }

        if (Input.GetMouseButtonDown(0))
        {
            if (selectionUI == null)
            {
                selectionUI = GetComponent<PlayerManager>().SelectionUI;
            }
            if (selectionUIScript == null)
            {
                selectionUIScript = selectionUI.GetComponent<SelectionInventoryUI>();
            }
            Interactable interactable = null;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            //if (Physics.Raycast(ray, out hit, 100, movementMask))
            //{
            //    agent.SetDestination(hit.point);
            //    
            //    if (agent.isStopped == true)
            //    {
            //        agent.isStopped = false;
            //    }

            //}
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }
            else
            {
                // if the mouse cast a ray
                if (Physics.Raycast(ray, out hit, 100))
                {
                    //check if we hit an interactable collider
                    interactable = hit.collider.GetComponent<Interactable>();
                    // if we did, and it has a unit health, set that as selection
                    if (interactable != null)
                    {
                        if (interactable.GetComponent<UnitHealth>() != null)
                        {                            
                            //close shopUI
                            selectionUI.SetActive(true);
                            if (ShopUI != null)
                            {
                                ShopUI.SetActive(false);
                            }                            
                            if (interactable != null)
                            {
                                GameObject unitSelected = interactable.gameObject;
                                selectionUIScript.SetSelection(unitSelected);

                                // get health
                                //if unit, get damage
                                //if player, get inventory, including tooltips

                            }
                        }
                        else if (interactable.gameObject.tag == "Shop")// if we hit a shop, open shop and close selection
                        {
                            interactable.ChangeActive(true);// open shopUI
                            ShopUI = interactable.gameObject.GetComponent<ShopInteractable>().ShopUI;
                            selectionUI.SetActive(false);// close selectionUI
                        }
                        
                        
                        
                        
                    }
                    // if we didn't hit an interactable, close shop and close selection
                    else if (interactable == null)
                    {
                        selectionUI.SetActive(false);
                        if (ShopUI != null)
                        {
                            ShopUI.SetActive(false);
                        }
                        
                    }




                }
            }




        }
        // If we have a target, set destination to the target position, face the target, and move towards them in remaining distance function
        if (target != null)
        {
            agent.SetDestination(target.position);            
            FaceTarget();
            RemainingDistanceFunction();
            
            //if (Vector3.Distance(target.position, transform.position) > 3f)
            //{
            //    agent.isStopped = false;
            //    agent.SetDestination(target.position);
            //    FaceTarget();
            //}
            //else
            //{
            //    agent.isStopped = true;
            //    RemoveFocus();
            //}

        }
        else if (target == null && droppingItem == false) // If we no longer have target and we aren't dropping an item, stop agent, stop speed, and remove focus if we haven't already
        {
            agent.isStopped = true;
            agent.speed = movementSpeed;
            agent.velocity = Vector3.zero;
            RemoveFocus();

        }
        else if (target == null && droppingItem == true) // If we are dropping item, keep moving toward the correct destination
        {
            RemainingDistanceFunction();
        }
        //if (Vector3.Distance(transform.position, agent.destination) < 0.5f)
        //{
        //    agent.isStopped = true;
        //}

        //EngineAudio();
    }

    private void RemainingDistanceFunction()
    {
        // If remaining distance is less than stopping distance and greater than stopping distance * 0.8
        if (agent.remainingDistance < agent.stoppingDistance && agent.remainingDistance > agent.stoppingDistance * 0.8f && Vector3.Distance(m_Rigidbody.velocity,Vector3.zero) >= 3)
        {
            // We want to lower speed so agent doesn't overshoot target
            // Slow movement speed by the amount of distance between remaining distance and stopping distance *0.8 divided by remaining distance
            float newSpeed = movementSpeed * ((agent.remainingDistance - (agent.stoppingDistance * 0.8f)) / agent.remainingDistance);
            // Don't lower speed lower than 1
            if (newSpeed < 1)
            {
                agent.speed = 1;
            }
            else
            {
                agent.speed = newSpeed;
            }

        }
        // if the remaining distance gets inside stopping distance *0.8
        else if (agent.remainingDistance < agent.stoppingDistance * 0.8f)
        {
            // If we are dropping item, drop it, and communicate that to the server
            if (droppingItem == true && target == null)
            {
                PhotonView.RPC("PunDropdownItemOnGround", PhotonTargets.AllBufferedViaServer);
                hasInteractedWithObject = true;
            }
            if (hasInteractedWithObject == true)
            {
                // Stop the agent and remove the focus, and fix the agent speed
                agent.isStopped = true;
                agent.velocity = Vector3.zero;
                RemoveFocus();
                agent.speed = movementSpeed;
            }

        }
    }
    [PunRPC]
    private void PunDropdownItemOnGround()
    {
        // Create pickup object, remove item from inventory, make sure item bool and item are reset
        PhotonNetwork.Instantiate(itemToBeDropped.itemPickupObject.name,new Vector3(agent.destination.x,itemToBeDropped.itemPickupObject.transform.position.y, agent.destination.z), itemToBeDropped.itemPickupObject.transform.rotation,0);
        PhotonView.RPC("PunRemove", PhotonTargets.AllViaServer,itemToBeDropped.dictionaryString);        
        itemToBeDropped = null;
        droppingItem = false;
    }

    // Face the target function
    private void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    // Used by right clicking normally, or left clicking when dropping a weapon
    private void SetFocus(Interactable newFocus)
    {
        // If the new focus passed in is not equal to the current focus set, defocus the last target in its interactable script and focus the new one, then follow it
        if (newFocus != focus)
        {
            if (focus != null)
            {
                focus.OnDefocused();
            }
            focus = newFocus;
            FollowTarget(newFocus);
        }
        hasInteractedWithObject = false;
        agent.speed = movementSpeed;                    // make sure movement speed is reset
        newFocus.OnFocused(transform,gameObject);       // This lets the interactable object know it is being focused and by whom
    }

    // Remove the focus of this object
    private void RemoveFocus()
    {
        if (focus != null)
        {
            focus.OnDefocused();        // Let the object know it has been defocused
        }        
        // Remove the focus and make sure you are not following anything
        focus = null;
        agent.speed = movementSpeed;
        StopFollowingTarget();
    }

    // Use the nav mesh to set the target, make sure agent isnt stopped, set the stopping distance. Having target set allows the update function to constantly update the destination
    private void FollowTarget(Interactable newTarget)
    {
        agent.isStopped = false;
        agent.updateRotation = false;
        agent.stoppingDistance = newTarget.radius * 0.8f;
        agent.acceleration = startingAcceleration;
        target = newTarget.interactionTransform;
    }

    // Stop the nav mesh agent and stop following the target
    private void StopFollowingTarget()
    {
        agent.stoppingDistance = 0;
        agent.isStopped = true;
        agent.updateRotation = true;
        target = null;
        agent.acceleration = startingAcceleration;
    }

    // If you click on the movement mask, tries to drop item there
    // If you click on an interactable, sets the focus to that interactable and essentially drops the item in it (currently just deletes the item)
    public void UseOrPlaceItem(Item item)
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        itemToBeDropped = item;
        droppingItem = true;
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            itemToBeDropped = null;
            droppingItem = false;
            return;
        }
        else
        {
            if (Physics.Raycast(ray, out hit, 100, movementMask))
            {
                agent.stoppingDistance = 5;
                agent.acceleration = startingAcceleration;
                agent.SetDestination(hit.point);

                //RemoveFocus();
                if (agent.isStopped == true)
                {
                    agent.isStopped = false;
                }


            }
            if (Physics.Raycast(ray, out hit, 100))
            {
                Interactable interactable = hit.collider.GetComponent<Interactable>();
                if (interactable != null)
                {
                    SetFocus(interactable);

                }


            }
        }



    }

    //private void EngineAudio()
    //{
    //    // If there is no input (the tank is stationary)...
    //    if (Mathf.Abs(m_MovementInputValue) < 0.1f && Mathf.Abs(m_TurnInputValue) < 0.1f)
    //    {
    //        // ... and if the audio source is currently playing the driving clip...
    //        if (m_MovementAudio.clip == m_EngineDriving)
    //        {
    //            // ... change the clip to idling and play it.
    //            m_MovementAudio.clip = m_EngineIdling;
    //            m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
    //            m_MovementAudio.Play();
    //        }
    //    }
    //    else
    //    {
    //        // Otherwise if the tank is moving and if the idling clip is currently playing...
    //        if (m_MovementAudio.clip == m_EngineIdling)
    //        {
    //            // ... change the clip to driving and play.
    //            m_MovementAudio.clip = m_EngineDriving;
    //            m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
    //            m_MovementAudio.Play();
    //        }
    //    }
    //}


    //private void FixedUpdate()
    //{
    //    //Adjust the rigidbodies position and orientation in FixedUpdate.
    //   Move();
    //}


    //private void Move()
    //{

    //    if (movement != Vector3.zero)
    //    {
    //        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movement.normalized), 0.2f);
    //    }
    //    transform.Translate(movement * movementSpeed * Time.deltaTime, Space.World);

    //}



}