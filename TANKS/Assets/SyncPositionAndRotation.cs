﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncPositionAndRotation : Photon.MonoBehaviour, IPunObservable {

    Rigidbody rBody;

    //Sync variables
    private float lastSynchronizationTime = 0f;
    private float syncDelay = 0f;
    private float syncTime = 0f;
    private Vector3 syncStartPosition = Vector3.zero;
    private Vector3 syncEndPosition = Vector3.zero;
    private Quaternion syncStartRotation = Quaternion.identity;
    private Quaternion syncEndRotation = Quaternion.identity;
    private bool receivedFirstUpdate;

    private void Awake()
    {
        rBody = GetComponent<Rigidbody>();
    }



    private void Update()
    {
        if (receivedFirstUpdate == true)
        {
            if (PhotonView.isMine)
            {
                return;
            }
            else
            {
                SyncedMovement();
            }
        }

        
    }

    private void SyncedMovement()
    {
        syncTime += Time.deltaTime;
        transform.localPosition = Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
        transform.localRotation = Quaternion.Lerp(syncStartRotation, syncEndRotation, syncTime / syncDelay);
    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //// We own this player: send the others our data

            //Sets the variables to send then sends them to other players

            // Position
            Vector3 pos = transform.localPosition;
            Quaternion rot = transform.localRotation;
            Vector3 velo = rBody.velocity;
            stream.Serialize(ref pos);
            stream.Serialize(ref rot);
            stream.Serialize(ref velo);


        }
        else
        {
            //// Network player, receive data

            // Receive latest state information
            Vector3 pos = Vector3.zero;
            Quaternion rot = Quaternion.identity;
            Vector3 velo = Vector3.zero;


            stream.Serialize(ref pos);
            stream.Serialize(ref rot);
            stream.Serialize(ref velo);


            // find sync
            syncTime = 0f;
            syncDelay = Time.time - lastSynchronizationTime;
            lastSynchronizationTime = Time.time;


            //update sync position and rotation
            syncEndPosition = pos + velo * syncDelay;
            syncEndRotation = rot;
            syncStartPosition = this.transform.localPosition;
            syncStartRotation = this.transform.localRotation;
            if (receivedFirstUpdate == false)
            {
                receivedFirstUpdate = true;
            }




        }
    }
}
