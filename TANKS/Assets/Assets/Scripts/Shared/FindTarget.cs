﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindTarget : Photon.MonoBehaviour, IPunObservable
{

    public GameObject target = null;
    public int targetPhotonID;

    private int teamNumber;
    private float followDistance;

    private void Awake()
    {
        followDistance = 20;//GetComponent<UnitManager>().followDistance;
    }

    void Start ()
    {
        if (PhotonView.isMine)
        {
            InvokeRepeating("UpdateTarget", 0f, 0.33f);
        }        
    }

    public void UpdateTarget()
    {
        // Makes sure that this unit has a team number set
        if (teamNumber == 0)
        {
            teamNumber = GetComponent<TeamNumber>().teamNumber;
        }

        // Only updates the target if there is no current target and the current target is outside follow distance
        if (target == null)
        {
            //Cycles through all gameobject with tag enemies
            //If a new enemy is closer than a previous and within range, it sets them as nearest enemy
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");                          // Creates empty array of units tagged with "Enemy"
            float shortestDistanceToEnemy = 50;                                                         // Only sets enemies within 50 as nearest enemy
            GameObject nearestEnemy = null;                                                             // Makes sure that the nearest enemy is reset each time

            foreach (GameObject enemy in enemies)
            {

                {
                    //  finds the distance to the enemy, and if the enemy is within follow distance, closer than any previously set enemy, and is not on same team as this object, set this enemy as the nearest enemy
                    float distanceToEnemy = Vector3.Distance(gameObject.transform.position, enemy.transform.position);

                    if (distanceToEnemy < shortestDistanceToEnemy &&
                        enemy != this.gameObject.transform.parent && 
                        distanceToEnemy <= followDistance &&
                        enemy.GetComponent<TeamNumber>().teamNumber != teamNumber)

                    {
                        shortestDistanceToEnemy = distanceToEnemy;
                        nearestEnemy = enemy;
                    }
                }

            }

            //Cycles through all gameobject with tag Player
            //If a new player is closer than a previous and within range, it sets them as nearest player
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");                             // Empty array of gameobjects with the tag of player
            float shortestDistanceToPlayer = 50;                                                            // Only sets players within 50 as nearest player
            GameObject nearestPlayer = null;                                                                // Makes sure that the nearest player is reset each time

            foreach (GameObject player in players)
            {

                {
                    // Finds the distance to the player, and if the player is within follow distance, clsoer than any previously set player, and is not on same team as this object, set this player as nearest player
                    float distanceToEnemy = Vector3.Distance(gameObject.transform.position, player.transform.position);

                    if (distanceToEnemy < shortestDistanceToPlayer && 
                        player != this.gameObject.transform.parent && 
                        distanceToEnemy <= followDistance &&
                        player.GetComponent<TeamNumber>().teamNumber != teamNumber)

                    {
                            shortestDistanceToPlayer = distanceToEnemy;
                            nearestPlayer = player;
                    }
                }

            }

            //Sets the nearest target to the gameobject that is closer between enemy unit or player.
            if (nearestPlayer != null && nearestEnemy != null)  // If there is both a player and enemy within range
            {
                // If the nearest player is closer than the nearest enemy, set nearest player as nearest target
                if (Vector3.Distance(nearestPlayer.transform.position, gameObject.transform.position) < Vector3.Distance(nearestEnemy.transform.position, gameObject.transform.position))
                {
                    target = nearestPlayer;
                }
                else // Nearest enemy is the nearest target
                {
                    target = nearestEnemy;
                }
            }
            else if (nearestPlayer != null) // If there is a nearest player, set the nearest player as nearest target
            {
                target = nearestPlayer;
            }
            else // If there is no nearest player, then the nearest enemy becomes the nearest target
            {
                target = nearestEnemy;
            }
            if (target != null)
            {
                targetPhotonID = target.GetComponent<PhotonView>().viewID;
            }
            
            
        }


    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //Target
            if (target != null)
            {
                int targetID = targetPhotonID;

                stream.Serialize(ref targetID);
            }
        }
        else
        {
            int targetID = 0;

            stream.Serialize(ref targetID);

            //Receive target ID, turn into target gameobject, if it is not already target gameobject
            if (targetID != 0 && targetID != targetPhotonID)
            {
                target = PhotonView.Find(targetID).gameObject;
                targetPhotonID = PhotonView.Find(targetID).viewID;
            }
        }


    }


    private void Update()
    {
        if (target == null)
        {
            return;
        }
        else
        {
            if (Vector3.Distance(target.transform.position, gameObject.transform.position) > followDistance)
            {
                Debug.Log("target out of range");
                target = null;
            }
        }
    }
}
